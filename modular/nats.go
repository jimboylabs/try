package main

import (
	"time"

	server "github.com/nats-io/nats-server/v2/server"
	nats "github.com/nats-io/nats.go"
)

type NatsOpts struct {
	ServerName      string
	ClientName      string
	JetStream       bool
	JetStreamDomain string
	IPC             bool
	Logging         bool
}

func EmbedNats(opts NatsOpts) (*nats.Conn, *server.Server, error) {
	serverOpts := &server.Options{
		ServerName:      opts.ServerName,
		DontListen:      opts.IPC,
		JetStream:       opts.JetStream,
		JetStreamDomain: opts.JetStreamDomain,
	}

	ns, err := server.NewServer(serverOpts)
	if err != nil {
		return nil, nil, err
	}

	if opts.Logging {
		ns.ConfigureLogger()
	}

	ns.Start()
	if !ns.ReadyForConnections(5 * time.Second) {
		return nil, nil, nats.ErrTimeout
	}

	clientOpts := []nats.Option{
		nats.Name(opts.ClientName),
	}
	if opts.IPC {
		clientOpts = append(clientOpts, nats.InProcessServer(ns))
	}
	nc, err := nats.Connect(nats.DefaultURL, clientOpts...)
	if err != nil {
		return nil, nil, err
	}

	return nc, ns, nil
}
