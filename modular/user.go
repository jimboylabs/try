package main

import (
	"net/http"

	nats "github.com/nats-io/nats.go"
)

var _ Module = (*UserModule)(nil)

type UserModule struct{}

// Init implements Module.
func (u *UserModule) Init(config map[string]any) error {
	return nil
}

// HTTPHandlers implements Module.
func (u *UserModule) HTTPHandlers(pub Publisher) []HTTPHandler {
	return []HTTPHandler{
		{
			Method: "POST",
			Path:   "/create",
			Handler: func(w http.ResponseWriter, r *http.Request) {
			},
		},
	}
}

// MsgHandlers implements Module.
func (u *UserModule) MsgHandlers(pub Publisher) []MsgHandler {
	return []MsgHandler{
		{
			Subject: "user.created",
			Handler: func(msg *nats.Msg) {
			},
		},
	}
}

// Name implements Module.
func (u *UserModule) Name() string {
	return "users"
}
