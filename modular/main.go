package main

import (
	"log"
	"net/http"

	nats "github.com/nats-io/nats.go"
)

type HTTPHandler struct {
	Method  string
	Path    string
	Handler http.HandlerFunc
}

type MsgHandler struct {
	Subject string
	Handler func(msg *nats.Msg)
}

type Publisher struct {
	nc *nats.Conn
}

func (p *Publisher) Publish(subject string, data []byte) error {
	msg := &nats.Msg{
		Subject: subject,
		Data:    data,
	}
	return p.nc.PublishMsg(msg)
}

type Module interface {
	Name() string
	Init(config map[string]any) error
	HTTPHandlers(pub Publisher) []HTTPHandler
	MsgHandlers(pub Publisher) []MsgHandler
}

func main() {
	nc, ns, err := EmbedNats(NatsOpts{
		ServerName:      "embeded-nats",
		ClientName:      "embeded-nats-client",
		JetStream:       true,
		JetStreamDomain: "example",
		IPC:             true,
		Logging:         true,
	})
	defer ns.WaitForShutdown()

	if err != nil {
		log.Fatal(err)
	}

	nc.Subscribe("u.system.test", func(m *nats.Msg) {
		m.Respond([]byte("Hello world"))
	})
}
