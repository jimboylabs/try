package entity

import (
	"time"

	"github.com/google/uuid"
)

type EventHeader struct {
	ID          string    `json:"id"`
	PublishedAt time.Time `json:"published_at"`
}

type HelloWorldSent struct {
	Header  EventHeader `json:"header"`
	Message string      `json:"message"`
}

func NewEventHeader() EventHeader {
	return EventHeader{
		ID:          uuid.NewString(),
		PublishedAt: time.Now().UTC(),
	}
}
