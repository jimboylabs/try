package main

import (
	"commer/config"
	"commer/entity"
	"commer/graph"
	"commer/message"
	"commer/storage/migrate"
	"context"
	"encoding/json"
	"errors"
	"net/http"
	"runtime"
	"time"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	commonHttp "github.com/ThreeDotsLabs/go-event-driven/common/http"
	"github.com/ThreeDotsLabs/go-event-driven/common/log"
	"github.com/ThreeDotsLabs/watermill"
	"github.com/ThreeDotsLabs/watermill-nats/v2/pkg/nats"
	watermillMessage "github.com/ThreeDotsLabs/watermill/message"
	"github.com/labstack/echo/v4"
	"github.com/nats-io/nats-server/v2/server"
	nc "github.com/nats-io/nats.go"
	"github.com/sirupsen/logrus"
	"golang.org/x/sync/errgroup"
)

func init() {
	log.Init(logrus.InfoLevel)
}

func run(ctx context.Context, cfg config.Config) error {
	logger := log.FromContext(ctx).WithFields(logrus.Fields{"status": "startup"})

	// -------------------------------------------------------------------------
	// GOMAXPROCS
	logger.Infof("GOMAXPROCS: %d", runtime.GOMAXPROCS(0))

	logger.Info("Starting service...")
	defer logger.Info("Shutdown complete!")

	// -------------------------------------------------------------------------
	// Database

	err := migrate.Run(ctx, cfg)
	if err != nil {
		logger.Errorf("Database migration failed: %s", err.Error())
	}

	// -------------------------------------------------------------------------
	// NATS Server

	serverOpts := &server.Options{
		DontListen: true,
		JetStream:  true,
	}

	ns, err := server.NewServer(serverOpts)
	if err != nil {
		return err
	}

	go ns.Start()

	if !ns.ReadyForConnections(5 * time.Second) {
		return errors.New("NATS Server timeout!")
	}

	// -------------------------------------------------------------------------
	// Watermill

	clientOpts := []nc.Option{
		nc.RetryOnFailedConnect(true),
		nc.Timeout(30 * time.Second),
		nc.ReconnectWait(1 * time.Second),
		nc.InProcessServer(ns),
	}

	marshaler := &nats.GobMarshaler{}

	subscribeOptions := []nc.SubOpt{
		nc.DeliverNew(),
		nc.AckExplicit(),
	}

	jsConfig := nats.JetStreamConfig{
		Disabled:         false,
		AutoProvision:    true,
		SubscribeOptions: subscribeOptions,
		TrackMsgId:       false,
		AckAsync:         false,
	}

	watermillLogger := log.NewWatermill(log.FromContext(ctx))

	watermillRouter := message.NewWatermillRouter(message.Config{
		ClientOpts:      clientOpts,
		Marshaler:       marshaler,
		JetstreamConfig: jsConfig,
		Logger:          watermillLogger,
		NatsServer:      ns,
	})

	errgrp, ctx := errgroup.WithContext(ctx)

	errgrp.Go(func() error {
		return watermillRouter.Run(ctx)
	})

	e := commonHttp.NewEcho()

	e.GET("/up", func(c echo.Context) error {
		publisher := message.NewNatsPublisher(message.Config{
			ClientOpts:      clientOpts,
			Marshaler:       marshaler,
			JetstreamConfig: jsConfig,
			Logger:          watermillLogger,
			NatsServer:      ns,
		})
		publisher = log.CorrelationPublisherDecorator{Publisher: publisher}

		event := entity.HelloWorldSent{
			Header:  entity.NewEventHeader(),
			Message: "Hello Jimmy!",
		}

		payload, err := json.Marshal(event)
		if err != nil {
			return err
		}

		msg := watermillMessage.NewMessage(watermill.NewUUID(), payload)
		msg.Metadata.Set("correlation_id", c.Request().Header.Get("Correlation-ID"))
		msg.Metadata.Set("type", "HelloWorldSent")

		err = publisher.Publish("HelloWorldSent", msg)

		return c.JSON(http.StatusOK, "ok")
	})

	srv := handler.NewDefaultServer(graph.NewExecutableSchema(graph.Config{Resolvers: &graph.Resolver{}}))

	e.GET("/playground", echo.WrapHandler(playground.Handler("GraphQL playground", "/query")))
	e.Any("/query", echo.WrapHandler(srv))

	errgrp.Go(func() error {
		<-watermillRouter.Running()

		err := e.Start(":3000")
		if err != nil && err != http.ErrServerClosed {
			return err
		}

		return nil
	})

	errgrp.Go(func() error {
		<-ctx.Done()

		ns.Shutdown()

		return e.Shutdown(context.Background())
	})

	return errgrp.Wait()
}

func main() {
	ctx := context.Background()

	// Initialize configuration
	cfg := config.InitConfig(context.Background())

	if err := run(ctx, cfg); err != nil {
		panic(err)
	}
}
