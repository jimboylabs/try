package config

import (
	"github.com/jmoiron/sqlx"
	"github.com/xo/dburl"
)

func NewDBConn(cfg *Config) (*sqlx.DB, error) {
	db, err := dburl.Open(cfg.Mysql.DatabaseURL)
	if err != nil {
		return nil, err
	}

	return sqlx.NewDb(db, "mysql"), nil
}
