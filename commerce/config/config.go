package config

import (
	"context"

	"github.com/joho/godotenv"
	"github.com/labstack/gommon/log"
	"github.com/sethvargo/go-envconfig"
)

type Config struct {
	ApiPort string `env:"PORT,default=3000"`
	GraphQL struct {
		PlaygroundEnabled bool `env:"GRAPHQL_PLAYGROUND_ENABLED,default=false"`
	}
	Mysql struct {
		RunMigrations bool   `env:"MYSQL_RUN_MIGRATIONS,default=true"`
		DatabaseURL   string `env:"MYSQL_DATABASE_URL,required"`
	}
}

func InitConfig(ctx context.Context) Config {
	if err := godotenv.Load(); err != nil {
		log.Print("Error loading .env file")
	}

	config := Config{}
	if err := envconfig.Process(ctx, &config); err != nil {
		log.Fatalf("%+v", err)
	}

	return config
}
