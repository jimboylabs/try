package message

import (
	"github.com/ThreeDotsLabs/watermill"
	"github.com/ThreeDotsLabs/watermill-nats/v2/pkg/nats"
	"github.com/nats-io/nats-server/v2/server"
	nc "github.com/nats-io/nats.go"
)

type Config struct {
	ClientOpts      []nc.Option
	Marshaler       nats.MarshalerUnmarshaler
	JetstreamConfig nats.JetStreamConfig
	Logger          watermill.LoggerAdapter

	NatsServer *server.Server
}
