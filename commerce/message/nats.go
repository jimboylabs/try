package message

import (
	"github.com/ThreeDotsLabs/watermill-nats/v2/pkg/nats"
	"github.com/ThreeDotsLabs/watermill/message"
)

func NewNatsPublisher(config Config) message.Publisher {
	publisher, err := nats.NewPublisher(
		nats.PublisherConfig{
			URL:         config.NatsServer.ClientURL(),
			NatsOptions: config.ClientOpts,
			Marshaler:   config.Marshaler,
			JetStream:   config.JetstreamConfig,
		},
		config.Logger,
	)
	if err != nil {
		panic(err)
	}

	return publisher
}
