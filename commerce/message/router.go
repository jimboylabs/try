package message

import (
	"commer/entity"
	"encoding/json"
	"fmt"

	"github.com/ThreeDotsLabs/watermill-nats/v2/pkg/nats"
	"github.com/ThreeDotsLabs/watermill/message"
)

func NewWatermillRouter(config Config) *message.Router {
	router, err := message.NewRouter(message.RouterConfig{}, config.Logger)
	if err != nil {
		panic(err)
	}

	// register middleware

	helloSub, err := nats.NewSubscriber(
		nats.SubscriberConfig{
			URL:         config.NatsServer.ClientURL(),
			NatsOptions: config.ClientOpts,
			Unmarshaler: config.Marshaler,
			JetStream:   config.JetstreamConfig,
		},
		config.Logger,
	)

	router.AddNoPublisherHandler("hello_world", "HelloWorldSent", helloSub, func(msg *message.Message) error {
		var event entity.HelloWorldSent
		err := json.Unmarshal(msg.Payload, &event)
		if err != nil {
			return err
		}

		fmt.Printf("Message received: %#v\n", event)

		return nil
	})

	return router
}
