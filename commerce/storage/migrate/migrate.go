package migrate

import (
	"commer/config"
	"context"
	"embed"
	"fmt"
	"net/url"

	"github.com/ThreeDotsLabs/go-event-driven/common/log"
	"github.com/amacneil/dbmate/v2/pkg/dbmate"
	_ "github.com/amacneil/dbmate/v2/pkg/driver/mysql"
	"github.com/sirupsen/logrus"
)

//go:embed *.sql
var migrationSQLs embed.FS

func Run(ctx context.Context, cfg config.Config) (err error) {
	logger := log.FromContext(ctx).WithFields(logrus.Fields{"status": "startup"})

	parsed, err := url.Parse(cfg.Mysql.DatabaseURL)
	if err != nil {
		return err
	}

	hostport := fmt.Sprintf("%s:%s", parsed.Hostname(), parsed.Port())

	logger.Infof("Starting database migration on hostport: %s", hostport)
	defer func() {
		if err != nil {
			logger.Infof("Finished database migration on hostport: %s", hostport)
		}
	}()

	dm := dbmate.New(parsed)
	dm.MigrationsDir = []string{"."}
	dm.FS = migrationSQLs

	migrations, err := dm.FindMigrations()
	if err != nil {
		return err
	}

	for _, m := range migrations {
		logger.Infof("Migrating... Version: %s: Filepath: %s", m.Version, m.FilePath)
	}

	return dm.CreateAndMigrate()
}
