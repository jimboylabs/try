<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Jobs\CreateFragment;
use Illuminate\Http\Request;

class FragmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store()
    {
        request()->validate([
            'ref_id' => ['required'],
            'title' => ['required', 'max:150'],
            'body' => ['required'],
        ]);

        CreateFragment::dispatchSync(request()->user(), request([
            'ref_id',
            'title',
            'body',
        ]));

        return response()->json([
            'msg' => 'success',
        ], 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
