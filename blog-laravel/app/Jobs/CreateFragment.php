<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateFragment implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(
        public User $user,
        public array $data,
    ){
        $this->user = $user->withoutRelations();
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        $payload = collect($this->data)->only([
            'title',
            'ref_id',
            'body',
        ])->toArray();

        $fragment = $this->user->fragments()->firstOrCreate(['ref_id' => $payload['ref_id']], $payload);

        if (!$fragment->wasRecentlyCreated) {
            $fragment->body = $payload['body'];
            $fragment->save();
        }
    }
}
