<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Fragment extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug',
        'body',
        'ref_id',
    ];

    protected function title(): Attribute
    {
        return Attribute::make(
            set: fn(string $value, array $attributes) => [
                'title' => $value,
                'slug' => Str::slug($value),
            ],
        );
    }

    protected static function booted(): void
    {
        static::saving(function (Fragment $model) {
            if ($model->isDirty('slug')) {
                $count = $model->where('slug', $model->slug)->count();
                if ($count > 0) {
                    $model->slug += '-'. ($count + 1);
                }
            }
        });
    }
}
