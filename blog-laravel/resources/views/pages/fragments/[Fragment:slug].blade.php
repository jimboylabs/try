<?php

use GrahamCampbell\Markdown\Facades\Markdown;
?>

<x-guest-layout>
    <section class="container mx-auto mt-8">
        <article class="bg-white">
            <h2 class="text-2xl font-bold mb-4">{{ $fragment->title }}</h2>
            {!! Markdown::convert($fragment->body)->getContent() !!}
        </article>
    </section>
</x-guest-layout>
