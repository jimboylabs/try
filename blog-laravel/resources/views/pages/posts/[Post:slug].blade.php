<x-guest-layout>
    <section class="container mx-auto mt-8">
        <article class="bg-white">
            <h2 class="text-2xl font-bold mb-4">{{ $post->title }}</h2>
            {!! tiptap_converter()->asHTML($post->body) !!}
        </article>
    </section>
</x-guest-layout>
