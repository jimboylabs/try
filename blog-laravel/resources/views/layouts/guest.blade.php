<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])
</head>

<body class="font-mono">
    <div class="container mx-auto px-8 py-8 lg:px-64 md:px-32 sm:px-16">
        <div class="mb-12 text-gray-800 flex justify-between items-center p-6" x-data="{ open: false }">
            <div>
                <h1 class="text-2xl font-bold">
                    <a href="/" wire:navigate>{{ config('app.name', 'Laravel') }}</a>
                </h1>
            </div>

            <!-- Default Menu -->
            <nav class="hidden md:flex">
                <a href="/" wire:navigate class="abbreviated-menu-item text-gray-800 hover:underline ml-2" data-full="ome">/h</a>
                <a href="/fragments" wire:navigate class="abbreviated-menu-item text-gray-800 hover:underline ml-2" data-full="ragments">/f</a>
                <a href="/projects" wire:navigate class="abbreviated-menu-item text-gray-800 hover:underline ml-2" data-full="rojects">/p</a>
                <a href="/about" wire:navigate class="abbreviated-menu-item text-gray-800 hover:underline ml-2" data-full="bout">/a</a>
            </nav>

            <div class="md:hidden" x-show="!open">
                <button class="items-center text-gray-800" @click="open = true">
                    <svg class="block h-4 w-4 fill-current" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <title>Mobile menu</title>
                        <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path>
                    </svg>
                </button>
            </div>

            <!-- Mobile Menu -->
            <div class="relative z-50" x-show="open" x-cloak>
                <nav class="fixed top-0 left-0 bottom-0 p-6 bg-white border-r overflow-y-auto">
                    <div class="flex flex-col">
                        <div class="flex items-center mb-8">
                            <button type="button" @click="open = false">
                                x
                            </button>
                        </div>
                        <ul>
                            <li>
                                <a href="/" wire:navigate class="block text-gray-800 hover:underline mb-2">/home</a>
                            </li>
                            <li>
                                <a href="/fragments" wire:navigate class="block text-gray-800 hover:underline mb-2">/fragments</a>
                            </li>
                            <li>
                                <a href="/projects" wire:navigate class="block text-gray-800 hover:underline mb-2">/projects</a>
                            </li>
                            <li>
                                <a href="/about" wire:navigate class="block text-gray-800 hover:underline mb-2">/about</a>
                            </li>
                        </ul>
                    </div>

                </nav>
            </div>
        </div>
        <section class="mb-12 p-6">
            {{ $slot }}
        </section>
        <footer class="text-gray-600 p-4 mt-8">
            <div class="container mx-auto text-center">
                <p>@wayanjimmy ~ &copy; 2024</p>
            </div>
        </footer>
    </div>
</body>

</html>
