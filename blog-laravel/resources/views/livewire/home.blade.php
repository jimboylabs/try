<?php

use App\Models\Post;

use function Livewire\Volt\{state, layout};

layout('layouts.guest');

state(['posts' => Post::all()]);
?>

<section class="container mx-auto mt-8">
    @foreach ($this->posts as $post)
        <article wire:key="{{ $post->id }}">
            <h2 class="text-xl font-bold mb-4 inline">
                <a class="text-black hover:underline" href="/posts/{{ $post->slug }}" wire:navigate>{{ $post->title }}</a>
            </h2>
            <span class="text-gray-700 inline ml-2 text-sm">{{ $post->created_at->format('Y-m-d') }}</span>
        </article>
    @endforeach
</section>
