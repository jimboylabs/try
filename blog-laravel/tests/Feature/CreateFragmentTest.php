<?php

it('it should not create fragment without token', function () {
    $response = $this->post('/api/fragments', [
        'title' => 'test',
        'slug' => 'test',
        'body' => 'test',
    ]);

    $response->assertStatus(401);
});
