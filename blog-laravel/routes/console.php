<?php

use App\Models\User;
use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');

Artisan::command('token', function () {
    $this->info("Generate token for user test@test.com");

    User::first()->update(['api_token' => 'qwe123']);

    $user = User::first();

    $this->info("Token generated: {$user->api_token}");
});

Artisan::command('create-filament-admin', function () {
    $this->info("Creating Filament Admin User");

    Artisan::call('make:filament-user', [
        '--name' => 'Admin',
        '--email' => env('FILAMENT_ADMIN_EMAIL'),
        '--password' => env('FILAMENT_ADMIN_PASSWORD')
    ]);
});
