<?php

use Illuminate\Http\Request;
use App\Http\Controllers\Api;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/hello', function (Request $request) {
    $info = [
        'X-From-Cdn' => $request->header('X-From-Cdn'),
        'Cf-Connecting-Ip' => $request->header('Cf-Connecting-Ip'),
        'X-Real-Ip' => $request->header('X-Real-Ip'),
        'X-Forwarded-For' => $request->header('X-Forwarded-For'),
        'ip' => $request->ip(),
        'ips' => $request->ips(),
    ];

    Log::info('Request headers ', $info);

    return response()->json($info);
});

Route::apiResource('fragments', Api\FragmentsController::class)
    ->only(['store'])
    ->middleware('auth:api');
