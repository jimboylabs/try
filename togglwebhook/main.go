package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"time"
	"togglwebhook/logging"
)

func encode[T any](w http.ResponseWriter, _ *http.Request, status int, v T) error {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	if err := json.NewEncoder(w).Encode(v); err != nil {
		return fmt.Errorf("encode json: %w", err)
	}
	return nil
}

func handleHealthzPlease(logger *slog.Logger) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		resp := map[string]string{
			"status": "ok",
		}
		err := encode(w, r, http.StatusOK, resp)
		if err != nil {
			logger.Error("failed to encode response", "error", err)
		}
	})
}

func handleWebhook(logger *slog.Logger) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		resp := map[string]string{
			"status": "ok",
		}
		err := encode(w, r, http.StatusOK, resp)
		if err != nil {
			logger.Error("failed to encode response", "error", err)
		}
	})
}

func addRoutes(mux *http.ServeMux, logger *slog.Logger) {
	mux.Handle("/healthz", handleHealthzPlease(logger))
	mux.Handle("/webhook", handleWebhook(logger))
}

func NewServer(logger *slog.Logger) http.Handler {
	mux := http.NewServeMux()
	addRoutes(mux, logger)
	var handler http.Handler = mux

	handler = logging.WithLogging(handler, logger)

	return handler
}

func run(ctx context.Context, w io.Writer, _ []string) error {
	ctx, cancel := signal.NotifyContext(ctx, os.Interrupt)
	defer cancel()

	logger := slog.New(slog.NewJSONHandler(w, nil))

	srv := NewServer(logger)

	Addr := ":8080"

	httpServer := &http.Server{
		Addr:    Addr,
		Handler: srv,
	}

	go func() {
		logger.Info("listening on", "addr", Addr)
		if err := httpServer.ListenAndServe(); err != nil {
			logger.Error("failed to start server", "error", err)
		}
	}()

	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()
		<-ctx.Done()
		shutdownCtx := context.Background()
		shutdownCtx, cancel := context.WithTimeout(shutdownCtx, 10*time.Second)
		defer cancel()
		if err := httpServer.Shutdown(shutdownCtx); err != nil {
			logger.Error("failed to shutdown server", "error", err)
		}
	}()
	wg.Wait()

	return nil
}

func main() {
	ctx := context.Background()
	if err := run(ctx, os.Stdout, os.Args); err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}
}
