<?php

use App\Http\Controllers\ViewController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('index');
});

Route::get('/{year}/{any?}', [ViewController::class, 'show'])
    ->where([
        'year' => '\d{4}',
        'any' => '.+',
    ]);
