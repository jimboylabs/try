## Ikigai

Menurutku Ikigai adalah sebuah sudut pandang yang bisa digunakan menerjemahkan hidup yang sangat kompleks ini menjadi sesuatu yang lebih mudah dimengerti. Lihat saja gambar dibawah ini.

![Screenshot](/static/20210103_120416_Ks0ouq.png)

Apa sih yang bikin kita bertahan mengerjakan sesuatu, mencintai sesuatu dan melakukan sesuatu untuk orang lain? Ketika melihat digambar diatas, aku jadi percaya kalau irisan antara _what you love_, _what are you good at_, _what you can be paid for_, dan _what the world needs_ bisa dijadikan semacam alasanku, mengapa aku harus tetap bertahan menjalani hidup.


## Mengapa perlu Ikigai?
