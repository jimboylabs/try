<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ $title ?? 'Wayan Jimmy'}}</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

        <!-- Scripts -->
        @vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>
    <body class="bg-white text-black font-mono lg:text-base sm:text-sm max-w-screen-md mx-auto">
      <header class="bg-white text-black py-2 px-4 lg:horizontal md:vertical">
        <nav class="container mx-auto px-2 py-2 center-v lg:w-1/2 md:w-full lg:text-left">
          <ul class="vertical">
            <li>
              <a
                href="/"
                class="hover:text-gray-600 transition-colors duration-200"
                wire:navigate
              >
                @wayanjimmy
              </a>
            </li>
            <li>
              <a
                href="/about"
                class="hover:text-gray-600 transition-colors duration-200"
                wire:navigate
              >
                about
              </a>
            </li>
          </ul>
        </nav>
        <div id="search" class="lg:w-1/2 sm:w-full"></div>
      </header>
      <main class="container mx-auto px-4 py-4 lg:text-base sm:text-sm">
        {{ $slot }}
      </main>
      <footer class="container mx-auto px-4 py-2">
        served by {{ $hostname }} with site version {{ $build }}
      </footer>
</html>
