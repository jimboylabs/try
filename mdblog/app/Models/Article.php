<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Article extends Model
{
    use HasFactory, \Sushi\Sushi;

    protected $casts = [
        'date' => 'date',
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('order', fn($query) => $query->orderBy('date', 'desc'));
    }

    protected $rows = [[
        'title' => 'Hello World!',
        'date' => '2024-05-05',
        'path' => '2024/hello',
        'description' => 'This is my first article',
    ]];

    protected function year(): Attribute
    {
        return Attribute::make(
            get: function () {
                $year = $this->date->format('Y');

                if ($year < 2024) {
                    return 'V old';
                }

                return $year;
            },
        );
    }

    protected function markdown(): Attribute
    {
        return Attribute::make(
            get: function () {
                return Storage::disk('views')
                    ->get("articles/{$this->path}.md");
            },
        );
    }
}
