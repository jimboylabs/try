<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Process;
use Illuminate\View\Component;

class HomeLayout extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(
        public string $hostname = 'localhost',
        public string $build = '',
    )
    {
        $this->hostname = Process::run('hostname')->output();
        $this->build = env('BUILD', 'develop');
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.home-layout');
    }
}
