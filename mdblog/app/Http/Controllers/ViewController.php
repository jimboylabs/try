<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ViewController extends Controller
{
    protected $disk;

    public function __construct()
    {
        $this->disk = Storage::disk('views');
    }

    public function show($year, $path = 'index')
    {
        $view = "{$year}/{$path}";

        if ($this->disk->exists($view . '.blade.php')) {
            return view($view, compact('view'));
        }

        return $this->article($view);
    }

    public function article($view)
    {
        $article = Article::where('path', $view)->first();

        if (!$article) {
            return 0;
        }

        return view('_post', [
            'view' => 'articles/' . $view,
            'article' => $article,
            'title' => $article->title,
            'date' => $article->date,
            'markdown' => $article->markdown,
        ]);
    }
}
