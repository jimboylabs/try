<?php

namespace App\Supports;

use Spatie\CommonMarkShikiHighlighter\HighlightCodeExtension;

class CustomHighlightCodeExtension extends HighlightCodeExtension
{
    public function __construct()
    {
        parent::__construct('github-light');
    }
}
