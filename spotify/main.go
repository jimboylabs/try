package main

import (
	"fmt"
	"os"

	"github.com/go-resty/resty/v2"
)

var baseURL = "https://api.spotify.com/v1"

type LoginResponse struct {
	AccessToken string `json:"access_token"`
	TokenType   string `json:"token_type"`
	ExpiresIn   int    `json:"expires_in"`
}

type SeachResponse struct {
	Artists struct {
		Href     string `json:"href"`
		Limit    int    `json:"limit"`
		Next     string `json:"next"`
		Offset   int    `json:"offset"`
		Previous string `json:"previous"`
		Total    int    `json:"total"`
		Items    []struct {
			ExternalUrls struct {
				Spotify string `json:"spotify"`
			} `json:"external_urls"`
			Followers struct {
				Href  string `json:"href"`
				Total int    `json:"total"`
			} `json:"followers"`
			Genres []string `json:"genres"`
			Href   string   `json:"href"`
			ID     string   `json:"id"`
			Images []struct {
				URL    string `json:"url"`
				Height int    `json:"height"`
				Width  int    `json:"width"`
			} `json:"images"`
			Name       string `json:"name"`
			Popularity int    `json:"popularity"`
			Type       string `json:"type"`
			URI        string `json:"uri"`
		} `json:"items"`
	} `json:"artists"`
}

func main() {
	clientID := os.Getenv("CLIENT_ID")
	clientSecret := os.Getenv("CLIENT_SECRET")

	client := resty.New()

	body := map[string]string{
		"grant_type":    "client_credentials",
		"client_id":     clientID,
		"client_secret": clientSecret,
	}

	var response LoginResponse

	_, err := client.R().SetFormData(body).SetResult(&response).Post("https://accounts.spotify.com/api/token")
	if err != nil {
		panic(err)
	}

	client.SetHeader("Authorization", fmt.Sprintf("Bearer %s", response.AccessToken))

	var searchResponse SeachResponse

	_, err = client.R().
		SetResult(&searchResponse).
		SetDebug(true).
		SetQueryString("q=yuika&type=artist").
		Get(fmt.Sprintf("%s/search", baseURL))
	if err != nil {
		panic(err)
	}

	fmt.Println(searchResponse.Artists.Items)
}
