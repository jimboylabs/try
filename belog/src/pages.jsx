export const title = "Braindump";
export const layout = "base.jsx";

export default ({ search }, { date }) => {
  const dateOptions = { year: "numeric", month: "2-digit", day: "2-digit" };

  return (
    <>
      <h2>Braindump</h2>
      <ul class="list-disc ml-4 mb-4">
        {search.pages("type=braindump !draft", "order date=desc").map(
          (dump) => {
            const url = dump.redirect_to ? dump.redirect_to : dump.url;
            return (
              <li>
                <time datetime={date(dump.date)} className="font-mono">
                  {dump.date.toLocaleDateString("en-US", dateOptions)}
                </time>{" "}
                - <a href={url}>{dump.title}</a>
              </li>
            );
          },
        )}
      </ul>
    </>
  );
};
