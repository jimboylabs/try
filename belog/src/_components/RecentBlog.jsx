export default function ({ search }, { date }) {
  const dateOptions = { year: "numeric", month: "2-digit", day: "2-digit" };
  const posts = search.pages("type=blog !draft", "order date=desc").slice(0, 5);

  return (
    <section class="my-6">
      <h2 class="font-bold mb-2">Blog</h2>
      <ul class="space-y-1">
        {posts.map((post) => {
          const url = post.redirect_to ? post.redirect_to : post.url;
          return (
            <li>
              <time datetime={date(post.date)} className="font-mono">
                {post.date.toLocaleDateString("en-US", dateOptions)}
              </time>{" "}
              - <a href={url}>{post.title}</a>
            </li>
          );
        })}
      </ul>
    </section>
  );
}
