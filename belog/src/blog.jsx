export const title = "Blog";
export const layout = "base.jsx";

export default ({ search }, { date }) => {
  const dateOptions = { year: "numeric", month: "2-digit", day: "2-digit" };

  return (
    <>
      <h2>Blog</h2>
      <ul class="list-disc ml-2 mb-2">
        {search.pages("type=blog !draft", "order date=desc").map((post) => {
          const url = post.redirect_to ? post.redirect_to : post.url;
          return (
            <li>
              <time datetime={date(post.date)} className="font-mono">
                {post.date.toLocaleDateString("en-US", dateOptions)}
              </time>{" "}
              - <a href={url}>{post.title}</a>
            </li>
          );
        })}
      </ul>
    </>
  );
};
