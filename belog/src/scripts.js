if (
  document.readyState === "complete" || document.readyState === "interactive"
) {
  setTimeout(onLoad, 0);
} else {
  document.addEventListener("DOMContentLoaded", onLoad);
}

function onLoad() {
  console.log("Hello World!");
}
