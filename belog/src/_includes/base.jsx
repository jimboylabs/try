import { os } from "/deps.js";

export default ({ title, children, build }) => (
  <html>
    <head>
      <script async src={`/scripts.js?${build}`}></script>
      <meta charset="UTF-8"></meta>
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      </meta>
      <link rel="stylesheet" href={`/styles.css?${build}`}></link>
      <link rel="stylesheet" href="/styles/shiki-extra/main.css" />
      <link
        rel="stylesheet"
        href="/styles/shiki-extra/transformerNotationDiff.css"
      />
      <link
        rel="stylesheet"
        href="/styles/shiki-extra/transformerNotationErrorLevel.css"
      />
      <link
        rel="stylesheet"
        href="/styles/shiki-extra/transformerNotationFocus.css"
      />
      <link
        rel="stylesheet"
        href="/styles/shiki-extra/transformerNotationHighlight.css"
      />
      <title>{title} - Jimmy</title>
    </head>
    <body class="bg-white text-black font-mono lg:text-base sm:text-sm max-w-screen-md mx-auto">
      <header class="bg-white text-black py-2 px-4 lg:horizontal md:vertical">
        <nav class="container mx-auto px-2 py-2 center-v lg:w-1/2 md:w-full lg:text-left">
          <ul class="vertical">
            <li>
              <a
                href="/"
                class="hover:text-gray-600 transition-colors duration-200"
              >
                @wayanjimmy
              </a>
            </li>
            <li>
              <a
                href="/blog"
                class="hover:text-gray-600 transition-colors duration-200"
              >
                blog
              </a>
            </li>
            <li>
              <a
                href="/pages"
                class="hover:text-gray-600 transition-colors duration-200"
              >
                braindump
              </a>
            </li>
            <li>
              <a
                href="/about"
                class="hover:text-gray-600 transition-colors duration-200"
              >
                about
              </a>
            </li>
          </ul>
        </nav>
        <div id="search" class="lg:w-1/2 sm:w-full"></div>
      </header>
      <main class="container mx-auto px-4 py-4 prose lg:text-base sm:text-sm">
        {children}
      </main>
      <footer class="container mx-auto px-4 py-2">
        served by {os.hostname()} with site version {build}
      </footer>
    </body>
  </html>
);
