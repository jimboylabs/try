package main

import (
	commonHTTP "belog/common/http"
	"belog/common/log"
	"context"
	"embed"
	"fmt"
	"io/fs"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/sirupsen/logrus"
	"golang.org/x/sync/errgroup"
)

//go:embed all:_site
var lumeFS embed.FS

func main() {
	log.Init(logrus.DebugLevel)

	ctx := context.Background()

	if err := run(ctx); err != nil {
		os.Exit(1)
	}
}

func getFilesystem(path string) http.FileSystem {
	fs, err := fs.Sub(lumeFS, path)
	if err != nil {
		panic(err)
	}

	return http.FS(fs)
}

func run(ctx context.Context) error {
	handler := commonHTTP.NewEcho()

	handler.Use(middleware.StaticWithConfig(middleware.StaticConfig{
		// Root:       "_site",
		Skipper: func(c echo.Context) bool {
			path := c.Path()
			return strings.HasPrefix(path, "/api")
		},
		HTML5:      true,
		Filesystem: getFilesystem("_site"),
	}))

	handler.GET("/api", func(c echo.Context) error {
		return c.String(http.StatusOK, "ok")
	})

	g, ctx := errgroup.WithContext(ctx)

	srv := &http.Server{
		Addr:    ":8080",
		Handler: handler,
	}

	g.Go(func() error {
		logrus.Info("status", "server started")

		err := srv.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			return err
		}

		return nil
	})

	// Wait for interrupt signal to gracefully shutdown the server
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)

	// Block until we receive the interrupt signal
	<-quit

	logrus.Info("status", "server shutting down")

	// Create a context with a timeout of 20 seconds for gracefully shutdown
	ctx, cancel := context.WithTimeout(ctx, 20*time.Second)
	defer cancel()

	// Initiate graceful shutdown with the timeout context
	if err := srv.Shutdown(ctx); err != nil {
		return fmt.Errorf("error during server shutdown: %w", err)
	}

	logrus.Info("status", "server stopped")

	if err := g.Wait(); err != nil {
		return fmt.Errorf("error during server shutdown: %w", err)
	}

	return nil
}
