package log

import (
	"context"

	"github.com/sirupsen/logrus"
)

type ctxKey int

const (
	loggerKey        ctxKey = iota // = 0
	correlationIDKey               // = 1
)

func Get(ctx context.Context) *logrus.Entry {
	log, ok := ctx.Value(loggerKey).(*logrus.Entry)
	if ok {
		return log
	}

	return logrus.NewEntry(logrus.StandardLogger())
}

func Set(ctx context.Context, log *logrus.Entry) context.Context {
	return context.WithValue(ctx, loggerKey, log)
}
