package log

import (
	"context"

	"github.com/lithammer/shortuuid/v3"
)

const CorrelationIDHttpHeader string = "Correlation-ID"

func SetCorrelationID(ctx context.Context, id string) context.Context {
	return context.WithValue(ctx, correlationIDKey, id)
}

func GetCorrelationID(ctx context.Context) string {
	v, ok := ctx.Value(correlationIDKey).(string)
	if ok {
		return v
	}

	Get(ctx).Warn("correlation ID not found in context")

	return "gen_" + shortuuid.New()
}
