package http

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/lithammer/shortuuid/v3"
)

func useMiddleware(e *echo.Echo) {
	e.Use(
		middleware.RequestIDWithConfig(middleware.RequestIDConfig{
			Generator: func() string {
				return shortuuid.New()
			},
		}),
	)
}
