import {
  date,
  esbuild,
  jsx_preact,
  lume,
  mdx,
  pagefind,
  postcss,
  shiki,
  shikiExtra,
  tailwindcss,
  typography,
} from "./deps.js";

import RecentBlog from "./src/_components/RecentBlog.jsx";

const site = lume({
  src: "./src",
  emptyDest: false,
})
  .use(date())
  .use(jsx_preact())
  .use(mdx({
    components: {
      "RecentBlog": RecentBlog,
    },
  }))
  .use(esbuild({
    options: {
      bundle: true,
      format: "iife",
      target: "es2015",
      minify: false,
      entryPoints: ["scripts.js"],
    },
  }))
  .use(tailwindcss({
    extensions: [".mdx", ".tsx", ".html", ".md", ".jsx"],
    options: {
      theme: {
        extend: {
          fontFamily: {
            mono: ["Roboto Mono", "monospace"],
          },
          typography: {
            prose: {
              fontSize: "14px",
            },
          },
        },
      },
      variants: {},
      plugins: [typography],
    },
  }))
  .use(postcss())
  .use(pagefind({
    ui: {
      containerId: "search",
      showImages: false,
      showEmptyFilters: true,
      resetStyles: true,
    },
  }))
  .use(shiki({
    highlighter: {
      langs: ["bash", "json", "yaml", "javascript", "typescript", "go", "rust"],
      themes: ["github-light"],
    },
    theme: "github-light",
  }))
  .use(shikiExtra({
    copyFiles: true,
  }));

site.copy("static/img");

const build = Deno.env.get("BUILD");
if (!build) {
  console.error("No BUILD environment variable set");
}
site.data("build", build || "develop");

export default site;
