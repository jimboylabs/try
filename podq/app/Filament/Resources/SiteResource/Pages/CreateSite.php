<?php

namespace App\Filament\Resources\SiteResource\Pages;

use App\Filament\Resources\SiteResource;
use Filament\Actions;
use Filament\Resources\Pages\CreateRecord;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Process;

class CreateSite extends CreateRecord
{
    protected static string $resource = SiteResource::class;

    protected function handleRecordCreation(array $data): Model
    {
        $name = $data['name'];

        //TODO: create repo
        $result = Process::run("ssh soft repo create {$name}");
        Log::info("result: {$result->output()}");

        // TODO: register webhook
        $app_url = config('app.url');
        $result = Process::run("ssh soft repo webhook create {$name} {$app_url}/api/webhook -e push");
        Log::info("result: {$result->output()}");

        $data['user_id'] = auth()->user()->id;

        return static::getModel()::create($data);
    }
}