<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Site extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected function repoUrl(): Attribute
    {
        return Attribute::make(
            get: fn () => "ssh://localhost:23231/{$this->name}.git",
        );
    }

    public function deployments(): HasMany
    {
        return $this->hasMany(Deployment::class);
    }
}
