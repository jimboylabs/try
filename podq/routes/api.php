<?php

use App\Enums\DeploymentStatus;
use App\Models\Site;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Process;
use Illuminate\Support\Facades\Route;

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:sanctum');

Route::post('/webhook', function (Request $request, Filesystem $fs) {
    $name = $request->input('repository.name');

    Log::info("request body: {content}", ["content" => $request->getContent()]);

    $site = Site::whereName($name)->firstOrFail();

    Log::info("prepare new deployment");

    $deployment = $site->deployments()->create([
        'status' => DeploymentStatus::PENDING,
        'request_body' => $request->getContent(),
    ]);

    Log::info("new deployment created: {$deployment->id}");

    $result = Process::path(base_path('.repo'))->run(
        <<<BASH
        git clone {$site->repo_url} "deploy-{$deployment->id}"
    BASH
    )->throw();

    $path = base_path(sprintf(".repo/deploy-%d", $deployment->id));

    if (!$fs->isDirectory($path)) {
        $fs->makeDirectory($path, 0755, true);
    }

    $sourceFile = resource_path('stubs/Dockerfile.stub');
    $destinationPath = $path . "/Dockerfile";

    $fs->copy($sourceFile, $destinationPath);

    Process::path($path)->run(
        <<<BASH
        podman build --format=docker -t podq/{$site->name}:latest .
    BASH
    )->throw();

    // TODO: ensure path exists and copy Dockerfile to the path

    // TODO: build container image using podman

    // TODO: push the image to the local registry

    if ($result->successful()) {
        $deployment->status = DeploymentStatus::SUCCESS;
        $deployment->output = $result->output();
        Log::info("deployment: {$deployment->id} succeed");
    } else {
        $deployment->status = DeploymentStatus::FAILED;
        $deployment->output = $result->errorOutput();
        Log::info("deployment: {$deployment->id} failed");
    }

    $deployment->save();

    return response()->json([
        'msg' => 'ok',
    ]);
});
