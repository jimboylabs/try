<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get("/", function () {
    return view("welcome");
});

Route::get("/subscription-checkout", function (Request $request) {
    Auth::loginUsingId(1);

    return $request
        ->user()
        ->newSubscription("default", "price_1QVrkgR1oH3qJkH4AAR8MUEd")
        ->trialDays(5)
        ->checkout([
            "success_url" => route("checkout-success"),
            "cancel_url" => route("checkout-cancel"),
        ]);
});

Route::get("/checkout/success", function (Request $request) {
    dd($request->all());
})->name("checkout-success");

Route::get("/checkout/cancel", function (Request $request) {
    dd($request->all());
})->name("checkout-cancel");

Route::post("/webhook", function (Request $request) {
    switch ($request->type) {
        case "customer.subscription.created":
        case "customer.subscription.updated":
        case "customer.subscription.deleted":
        case "customer:updated":
        case "customer:deleted":
        case "payment_method.automatically_updated":
        case "invoice.payment_action_required":
        case "invoice.payment_succeeded":
        default:
            return response()->json(["message" => "Something went wrong"], 500);
    }
});
