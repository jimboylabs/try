import { AuthPage } from "@refinedev/mui";

export default function Register() {
  return <AuthPage type="register" />;
}
