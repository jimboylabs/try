package main

import (
	"context"
	"errors"
	"log"
	"time"

	"github.com/ThreeDotsLabs/watermill"
	watermillNats "github.com/ThreeDotsLabs/watermill-nats/v2/pkg/nats"
	"github.com/ThreeDotsLabs/watermill/message"
	"github.com/nats-io/nats-server/v2/server"
	"github.com/nats-io/nats.go"
)

func main() {
	serverOpts := &server.Options{
		DontListen: true,
		JetStream:  true,
	}

	ns, err := RunEmbeddedServer(serverOpts)
	if err != nil {
		log.Fatal(err)
	}

	clientOpts := []nats.Option{
		nats.RetryOnFailedConnect(true),
		nats.Timeout(30 * time.Second),
		nats.ReconnectWait(1 * time.Second),
		nats.InProcessServer(ns),
	}

	natsURL := ns.ClientURL()
	marshaler := &watermillNats.GobMarshaler{}

	logger := watermill.NewStdLogger(false, false)

	subscribeOptions := []nats.SubOpt{
		nats.DeliverNew(),
		nats.AckExplicit(),
	}

	jsConfig := watermillNats.JetStreamConfig{
		Disabled:         false,
		AutoProvision:    true,
		SubscribeOptions: subscribeOptions,
		TrackMsgId:       false,
		AckAsync:         false,
	}

	subscriber, err := watermillNats.NewSubscriber(
		watermillNats.SubscriberConfig{
			URL:         natsURL,
			NatsOptions: clientOpts,
			//CloseTimeout:   30 * time.Second,
			//AckWaitTimeout: 30 * time.Second,
			Unmarshaler: marshaler,
			JetStream:   jsConfig,
		},
		logger,
	)
	if err != nil {
		panic(err)
	}

	messages, err := subscriber.Subscribe(context.Background(), "example_topic")
	if err != nil {
		panic(err)
	}

	go processJS(messages)

	publisher, err := watermillNats.NewPublisher(
		watermillNats.PublisherConfig{
			URL:         ns.ClientURL(),
			NatsOptions: clientOpts,
			Marshaler:   marshaler,
			JetStream:   jsConfig,
		},
		logger,
	)
	if err != nil {
		panic(err)
	}

	msg := message.NewMessage(watermill.NewUUID(), []byte("Hello world"))
	if err := publisher.Publish("example_topic", msg); err != nil {
		panic(err)
	}

	ns.WaitForShutdown()
}

func processJS(messages <-chan *message.Message) {
	for msg := range messages {
		log.Printf("received message: %s, payload: %s", msg.UUID, string(msg.Payload))

		msg.Ack()
	}
}

func RunEmbeddedServer(serverOpts *server.Options) (*server.Server, error) {
	ns, err := server.NewServer(serverOpts)
	if err != nil {
		return nil, err
	}

	ns.ConfigureLogger()

	go ns.Start()

	if !ns.ReadyForConnections(5 * time.Second) {
		return nil, errors.New("NATS Server timeout")
	}

	return ns, nil
}
