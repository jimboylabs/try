# Try

The code in this project is a mix of:

- Code snippets from tutorials and books
- Small experiments and prototypes
- Examples and demos
- Re-implementations of existing projects for learning
