package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"log/slog"
	"net/http"
	"os"
	"strings"
)

type AlertPayload struct {
	InvolvedObject struct {
		Kind            string `json:"kind"`
		Namespace       string `json:"namespace"`
		Name            string `json:"name"`
		UID             string `json:"uid"`
		APIVersion      string `json:"apiVersion"`
		ResourceVersion string `json:"resourceVersion"`
	} `json:"involvedObject"`
	Severity            string            `json:"severity"`
	Timestamp           string            `json:"timestamp"`
	Message             string            `json:"message"`
	Reason              string            `json:"reason"`
	Metadata            map[string]string `json:"metadata"`
	ReportingController string            `json:"reportingController"`
	ReportingInstance   string            `json:"reportingInstance"`
}

func main() {
	handler := slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{
		Level: slog.LevelDebug,
	})
	logger := slog.New(handler)
	slog.SetDefault(logger)

	forwardURL := os.Getenv("FORWARD_URL")
	if forwardURL == "" {
		log.Fatal("FORWARD_URL environment variable is not set")
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodPost {
			http.Error(w, "Only POST method is allowed", http.StatusMethodNotAllowed)
			return
		}

		var payload AlertPayload
		err := json.NewDecoder(r.Body).Decode(&payload)
		if err != nil {
			http.Error(w, "Error parsing JSON request body", http.StatusBadRequest)
			return
		}

		slog.Info("Received payload", "payload", payload)

		if strings.Contains(payload.Message, "Reconciliation finished") {
			fmt.Fprintf(w, "Skip Message Forwarding")
			return
		}

		message := fmt.Sprintf("%s/%s/%s\n", payload.InvolvedObject.Kind, payload.InvolvedObject.Namespace, payload.InvolvedObject.Name)
		message += fmt.Sprintf("%s\n", payload.Message)
		revision, ok := payload.Metadata["revision"]
		if ok {
			message += fmt.Sprintf("Revision: %s\n", revision)
		}

		err = forwardMessage(forwardURL, message)
		if err != nil {
			slog.Info("Error forwarding message", "error", err)
			http.Error(w, "Error forwarding message", http.StatusInternalServerError)
			return
		}

		fmt.Fprintf(w, "Message forwarded successfully")
	})

	log.Println("Server starting on :8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func forwardMessage(url, message string) error {
	req, err := http.NewRequest(http.MethodPost, url, strings.NewReader(message))
	if err != nil {
		return err
	}

	username := os.Getenv("FORWARD_USERNAME")
	password := os.Getenv("FORWARD_PASSWORD")

	if username != "" && password != "" {
		slog.Info("Using basic auth")
		req.SetBasicAuth(username, password)
	}

	req.Header.Set("Content-Type", "text/plain")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		body, _ := io.ReadAll(resp.Body)
		return fmt.Errorf("forward request failed with status %d: %s", resp.StatusCode, string(body))
	}

	return nil
}
