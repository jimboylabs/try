import os
import logging
from telegram import Update
from telegram.ext import Application, CommandHandler, MessageHandler, filters, CallbackContext
import subprocess

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)
logger = logging.getLogger(__name__)

TELEGRAM_API_TOKEN = os.environ.get('TELEGRAM_API_TOKEN')

def start(update: Update, context: CallbackContext) -> None:
    update.message.reply_text('Hi! Send me a URL and I will download the media for you.')

def download_media(url: str, download_folder: str) -> str:
    command = ['you-get', '-o', download_folder, url]
    subprocess.run(command, check=True)

    files = os.listdir(download_folder)
    if files:
        return os.path.join(download_folder, files[0])
    else:
        return None

async def handle_message(update: Update, context: CallbackContext) -> None:
    url = update.message.text
    download_folder = 'downloads'
    os.makedirs(download_folder, exist_ok=True)

    try:
        media_path = download_media(url, download_folder)
        if media_path:
            await update.message.reply_text('Media downloaded successfully. Sending it to you...')
            try:
                with open(media_path, 'rb') as file:
                    await update.message.reply_document(document=file)
                os.remove(media_path)
            except Exception as e:
                await update.message.reply_text(f'Error sending media: {str(e)}')
        else:
            update.message.reply_text('Failed to download media.')
    except subprocess.CalledProcessError as e:
        update.message.reply_text(f'Error downloading media: {str(e)}')

def main() -> None:
    logger.info('Starting bot...')
    application = Application.builder().token(TELEGRAM_API_TOKEN).build()

    application.add_handler(CommandHandler("start", start))
    application.add_handler(MessageHandler(filters.TEXT & ~filters.COMMAND, handle_message))

    logger.info('Bot started polling')
    application.run_polling(allowed_updates=Update.ALL_TYPES)

if __name__ == '__main__':
    main()