import type { AuthActionResponse, AuthProvider, CheckResponse, OnErrorResponse } from "@refinedev/core";

export const TOKEN_KEY = "refine-auth";

export const authProvider: AuthProvider = {
  login: async function (params: any): Promise<AuthActionResponse> {
    const response = await fetch('/api/token', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        id: "1",
        email: params.email,
      }),
    });

    if (!response.ok) {
      throw new Error('Failed to login');
    }

    const data = await response.json();

    localStorage.setItem(TOKEN_KEY, data.token);

    console.log({ token: data.token });

    return {
      success: true,
      redirectTo: "/",
    };
  },
  logout: async function (params: any): Promise<AuthActionResponse> {
    localStorage.removeItem(TOKEN_KEY);

    return {
      success: true,
      redirectTo: "/login",
    };
  },
  check: async function (params?: any): Promise<CheckResponse> {
    const token = localStorage.getItem(TOKEN_KEY);
    if (token) {
      return {
        authenticated: true,
      };
    }

    return {
      authenticated: false,
      redirectTo: "/login",
    };
  },
  onError: async function (error: any): Promise<OnErrorResponse> {
    console.error(error);
    return { error };
  }
};
