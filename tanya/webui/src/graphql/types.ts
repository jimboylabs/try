import type * as Types from './schema.types';

export type CategoriesListQueryVariables = Types.Exact<{
  offset: Types.Scalars['Int']['input'];
  limit: Types.Scalars['Int']['input'];
  order_by?: Types.InputMaybe<Array<Types.Categories_Order_By> | Types.Categories_Order_By>;
  where?: Types.InputMaybe<Types.Categories_Bool_Exp>;
}>;


export type CategoriesListQuery = { categories: Array<Pick<Types.Categories, 'id' | 'name' | 'user_id' | 'created_at' | 'updated_at'>>, categories_aggregate: { aggregate?: Types.Maybe<Pick<Types.Categories_Aggregate_Fields, 'count'>> } };

export type CategoryShowQueryVariables = Types.Exact<{
  id: Types.Scalars['Int']['input'];
}>;


export type CategoryShowQuery = { categories_by_pk?: Types.Maybe<Pick<Types.Categories, 'id' | 'name' | 'user_id' | 'created_at'>> };

export type CategoryCreateMutationVariables = Types.Exact<{
  object: Types.Categories_Insert_Input;
}>;


export type CategoryCreateMutation = { insert_categories_one?: Types.Maybe<Pick<Types.Categories, 'id'>> };

export type CategoryUpdateMutationVariables = Types.Exact<{
  id: Types.Scalars['Int']['input'];
  object: Types.Categories_Set_Input;
}>;


export type CategoryUpdateMutation = { update_categories_by_pk?: Types.Maybe<Pick<Types.Categories, 'id' | 'name' | 'user_id' | 'created_at' | 'updated_at'>> };

export type CategoryDeleteMutationVariables = Types.Exact<{
  id: Types.Scalars['Int']['input'];
}>;


export type CategoryDeleteMutation = { delete_categories_by_pk?: Types.Maybe<Pick<Types.Categories, 'id'>> };
