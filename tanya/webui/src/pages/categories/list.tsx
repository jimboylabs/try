import { DataGrid, type GridColDef } from "@mui/x-data-grid";
import {
  DeleteButton,
  EditButton,
  List,
  ShowButton,
  useDataGrid,
} from "@refinedev/mui";
import React from "react";
import type { CategoriesListQuery } from "../../graphql/types"
import type { GetFieldsFromList } from "@refinedev/hasura"
import { CATEGORIES_LIST_QUERY, CATEGORY_DELETE_MUTATION } from "./queries";

export const CategoryList = () => {
  const { dataGridProps } = useDataGrid<GetFieldsFromList<CategoriesListQuery>>({
    sorters: {
      initial: [
        {
          field: "id",
          order: "asc",
        }
      ]
    },
    filters: {
      initial: []
    },
    meta: {
      gqlQuery: CATEGORIES_LIST_QUERY,
    },
  });

  const columns = React.useMemo<GridColDef[]>(
    () => [
      {
        field: "id",
        headerName: "ID",
        type: "number",
        minWidth: 50,
      },
      {
        field: "name",
        flex: 1,
        headerName: "Name",
        minWidth: 200,
      },
      {
        field: "created_at",
        flex: 1,
        headerName: "Created At",
        minWidth: 200,
        valueFormatter: (params) => {
          return new Date(params.value).toLocaleString();
        }
      },
      {
        field: "actions",
        headerName: "Actions",
        sortable: false,
        renderCell: function render({ row }) {
          return (
            <>
              <EditButton hideText recordItemId={row.id} />
              <ShowButton hideText recordItemId={row.id} />
              <DeleteButton hideText recordItemId={row.id} meta={{
                gqlMutation: CATEGORY_DELETE_MUTATION,
              }} />
            </>
          );
        },
        align: "center",
        headerAlign: "center",
        minWidth: 80,
      },
    ],
    []
  );

  return (
    <List>
      <DataGrid {...dataGridProps} columns={columns} autoHeight />
    </List>
  );
};
