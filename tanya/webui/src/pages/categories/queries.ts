import gql from "graphql-tag";

export const CATEGORIES_LIST_QUERY = gql`
query CategoriesList (
  $offset: Int!
  $limit: Int!
  $order_by: [categories_order_by!]
  $where: categories_bool_exp
){
  categories(offset: $offset, limit: $limit, order_by: $order_by, where: $where) {
    id
    name
    user_id
    created_at
    updated_at
  }
  categories_aggregate(where: $where) {
    aggregate {
      count
    }
  }
}
`;

export const CATEGORY_SHOW_QUERY = gql`
query CategoryShow (
  $id: Int!
){
  categories_by_pk(id: $id) {
    id
    name
    user_id
    created_at
  }
}`

export const CATEGORY_CREATE_MUTATION = gql`
mutation CategoryCreate ($object: categories_insert_input!) {
  insert_categories_one(object: $object) {
    id
  }
}`

export const CATEGORY_UPDATE_MUTATION = gql`
mutation CategoryUpdate (
  $id: Int!
  $object: categories_set_input!
) {
  update_categories_by_pk(pk_columns: {id: $id}, _set: $object) {
    id
    name
    user_id
    created_at
    updated_at
  }
}`

export const CATEGORY_DELETE_MUTATION = gql`
mutation CategoryDelete (
  $id: Int!
) {
  delete_categories_by_pk(id: $id) {
    id
  }
}`
