import { Stack, Typography } from "@mui/material";
import { useShow } from "@refinedev/core";
import { DeleteButton, EditButton, ListButton, RefreshButton, Show, TextFieldComponent as TextField } from "@refinedev/mui";
import { CATEGORY_SHOW_QUERY, CATEGORY_DELETE_MUTATION } from "./queries";
import { GetFields } from "@refinedev/hasura";
import { CategoryShowQuery } from "../../graphql/types";

export const CategoryShow = () => {
  const { query } = useShow<GetFields<CategoryShowQuery>>({
    meta: {
      gqlQuery: CATEGORY_SHOW_QUERY,
    },
  });
  const { data, isLoading } = query;

  const record = data?.data;

  return (
    <Show isLoading={isLoading} headerButtons={({ deleteButtonProps, editButtonProps, listButtonProps, refreshButtonProps }) => (
      <>
        {listButtonProps && <ListButton {...listButtonProps} />}
        {editButtonProps && <EditButton {...editButtonProps} />}
        {deleteButtonProps && <DeleteButton {...deleteButtonProps} meta={{ gqlMutation: CATEGORY_DELETE_MUTATION }} />}
        {refreshButtonProps && <RefreshButton {...refreshButtonProps} />}
      </>
    )}>
      <Stack gap={1}>
        <Typography variant="body1" fontWeight="bold">
          {"ID"}
        </Typography>
        <TextField value={record?.id} />
        <Typography variant="body1" fontWeight="bold">
          {"Name"}
        </Typography>
        <TextField value={record?.name} />
      </Stack>
    </Show>
  );
};
