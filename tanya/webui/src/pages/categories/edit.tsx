import { Box, TextField } from "@mui/material";
import { Edit } from "@refinedev/mui";
import { useForm } from "@refinedev/react-hook-form";
import { CATEGORY_UPDATE_MUTATION, CATEGORY_SHOW_QUERY } from "./queries";
import { GetFields } from "@refinedev/hasura";
import { CategoryUpdateMutation } from "../../graphql/types";
import { FieldValues } from "react-hook-form";

type FormValues = GetFields<CategoryUpdateMutation>

export const CategoryEdit = () => {
  const {
    saveButtonProps,
    register,
    formState: { errors },
    refineCore: { onFinish },
    handleSubmit
  } = useForm<FormValues>({
    refineCoreProps: {
      meta: {
        gqlMutation: CATEGORY_UPDATE_MUTATION,
        gqlQuery: CATEGORY_SHOW_QUERY,
      },
    },
  });

  const onFinishHandler = (values: FieldValues) => {
    return onFinish({
      ...values,
      updated_at: new Date().toISOString(),
    });
  };

  return (
    <Edit saveButtonProps={{
      ...saveButtonProps,
      onClick: handleSubmit(onFinishHandler),
    }}>
      <Box
        component="form"
        sx={{ display: "flex", flexDirection: "column" }}
        autoComplete="off"
      >
        <TextField
          {...register("name", {
            required: "This field is required",
          })}
          error={!!(errors as any)?.title}
          helperText={(errors as any)?.title?.message}
          margin="normal"
          fullWidth
          InputLabelProps={{ shrink: true }}
          type="text"
          label={"Name"}
          name="name"
        />
      </Box>
    </Edit>
  );
};
