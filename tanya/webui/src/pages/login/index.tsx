import { AuthPage } from "@refinedev/mui";

export const Login = () => {
  return (
    <AuthPage
      type="login"
      formProps={{
        defaultValues: { email: "john.doe@hasura.io", password: "demodemo" },
      }}
    />
  );
};
