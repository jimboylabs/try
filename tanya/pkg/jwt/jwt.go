package jwt

import (
	"fmt"
	"net/http"
	"strconv"
	"tanya/entities"
	"time"

	"github.com/dgrijalva/jwt-go"
)

type Service struct {
	JwtSecret string
}

func NewService(jwtSecret string) *Service {
	return &Service{
		JwtSecret: jwtSecret,
	}
}

func (ths *Service) CreateAccessToken(user entities.User, userId int64, r *http.Request) (string, error) {
	roles := append(user.Authorities, "anonymous", "OPS")
	claims := jwt.MapClaims{
		"sub":                    user.Email,
		"exp":                    time.Now().Add(60 * 24 * time.Minute).Unix(),
		"iss":                    r.URL.String(),
		"x-hasura-allowed-roles": roles,
		"x-hasura-default-role":  "OPS",
		"x-hasura-user-id":       fmt.Sprintf("%d", user.ID),
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString([]byte(ths.JwtSecret))
}

func (ths *Service) CreateRefreshToken(user entities.User, r *http.Request) (string, error) {
	roles := append(user.Authorities, "anonymous", "OPS")
	claims := jwt.MapClaims{
		"sub":                    user.Email,
		"exp":                    time.Now().Add(30 * time.Minute).Unix(),
		"iss":                    r.URL.String(),
		"x-hasura-allowed-roles": roles,
		"x-hasura-default-role":  "OPS",
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString([]byte(ths.JwtSecret))
}

func (ths *Service) RefreshAccessToken(r *http.Request) (map[string]string, error) {
	refreshToken := r.Header.Get("Authorization")[7:]
	token, err := jwt.Parse(refreshToken, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(ths.JwtSecret), nil
	})

	if err != nil {
		return nil, err
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		email := claims["sub"].(string)
		userIDStr := claims["x-hasura-user-id"].(string)
		userID, err := strconv.ParseInt(userIDStr, 10, 64)
		if err != nil {
			return nil, fmt.Errorf("invalid user ID: %v", err)
		}

		accessToken, err := ths.CreateAccessToken(entities.User{
			ID:          userID,
			Email:       email,
			Authorities: []string{"anonymous"},
		}, userID, r)
		if err != nil {
			return nil, err
		}

		tokens := map[string]string{
			"access_token":  accessToken,
			"refresh_token": refreshToken,
		}
		return tokens, nil
	}

	return nil, fmt.Errorf("invalid token")
}
