package main

import (
	"embed"
	"io/fs"
	"log"
	"net/http"
	"os"
	"strconv"
	"tanya/entities"
	"tanya/pkg/jwt"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

//go:embed all:webui/dist
var refineFS embed.FS

func main() {
	distFS, err := fs.Sub(refineFS, "webui/dist")
	if err != nil {
		log.Fatal(err)
	}

	refineHandler := http.FileServer(http.FS(distFS))

	e := echo.New()

	e.Use(middleware.Logger())

	e.GET("/", echo.WrapHandler(refineHandler))
	e.GET("/assets/*", echo.WrapHandler(refineHandler))

	e.POST("/api/token", func(c echo.Context) error {
		request := struct {
			ID    string `json:"id"`
			Email string `json:"email"`
		}{}

		if err := c.Bind(&request); err != nil {
			return err
		}

		userID, err := strconv.ParseInt(request.ID, 10, 64)
		if err != nil {
			return err
		}

		jwtSvc := jwt.NewService(os.Getenv("JWT_SECRET"))
		token, err := jwtSvc.CreateAccessToken(entities.User{
			ID:          userID,
			Email:       request.Email,
			Authorities: []string{},
		}, 1, c.Request())
		if err != nil {
			return err
		}

		return c.JSON(http.StatusOK, map[string]any{"token": token})
	})

	e.Any("/*", func(c echo.Context) error {
		return c.Redirect(http.StatusFound, "/")
	})

	e.Logger.Fatal(e.Start(":3000"))
}
