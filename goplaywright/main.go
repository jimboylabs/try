package main

import (
	"log"

	"github.com/playwright-community/playwright-go"
)

func main() {
	pw, err := playwright.Run(&playwright.RunOptions{
		SkipInstallBrowsers: true,
	})
	if err != nil {
		log.Fatal(err)
	}

	browser, err := pw.Chromium.Connect("ws://localhost:3000")
	if err != nil {
		log.Fatal(err)
	}

	_ = browser
}
