import { redirect} from "react-router";

const localStorageKey = '__pocketurso_token__';

export function setToken(token: string): void {
  window.localStorage.setItem(localStorageKey, token);
}

export function getToken(): string | null {
  return window.localStorage.getItem(localStorageKey);
}

export function isAuthenticated(): boolean {
  return !!getToken();
}

export function logout(): void {
  window.localStorage.removeItem(localStorageKey);
}
