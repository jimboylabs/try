import Button from "@mui/material/Button";

export function Welcome() {
  return (
    <main className="flex items-center justify-center pt-16 pb-4">
      <Button variant="contained">Hello World</Button>
    </main>
  );
}