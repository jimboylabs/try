import Stack from "@mui/material/Stack";
import NotificationRoundedIcon from "@mui/icons-material/NotificationsRounded";
import MenuButton from "./MenuButton";

export default function Header() {
  return (
    <Stack direction="row" sx={{
      display: { xs: "none", md: "flex" },
      width: '100%',
      alignmentItems: { xs: 'flex-start', md: 'center' },
      justifyContent: 'space-between',
      maxWidth: { sm: '100%', md: '1700px' },
      pt: 1.5,
    }} spacing={2}>
      <Stack direction="row" sx={{ gap: 1 }}>
        <MenuButton showBadge={false}>
          <NotificationRoundedIcon />
        </MenuButton>
      </Stack>
    </Stack>
  );
}
