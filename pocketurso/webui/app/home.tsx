import type { Route } from "./routes/+types/home";
import Box from "@mui/material/Box";
import Stack from "@mui/material/Stack";

import SideMenu from "./components/SideMenu";
import Header from "./components/Header";
import AppNavbar from "./components/AppNavbar";

export function meta({ }: Route.MetaArgs) {
  return [
    { title: "New React Router App" },
    { name: "description", content: "Welcome to React Router!" },
  ];
}

export default function Home() {
  return (
    <Box sx={{ display: "flex" }}>
      <SideMenu />
      <AppNavbar />
      <Box component="main" sx={{ flexGrow: 1, overflow: "auto" }}>
        <Stack spacing={2} sx={{
          alignItems: "center",
          mx: 3,
          pb: 5,
          mt: { xs: 8, md: 0 }
        }}>
          <Header />
          <div>Main grid</div>
        </Stack>
      </Box>
    </Box>
  );
}
