import {
  index,
  layout,
  route,
  type RouteConfig,
} from "@react-router/dev/routes";

export default [
  index("./home.tsx"),

  route("login", "./auth/login.tsx"),
] satisfies RouteConfig;
