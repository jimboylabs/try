import * as log from "@std/log";
import { Context } from "grammy";
import { InputFile, InputMediaBuilder } from "grammy";
import { InputMediaPhoto, InputMediaVideo } from "grammy_types";

import { DownloadResult } from "./download.ts";
import * as helper from "./helper.ts";

const MEDIA_TYPE_PHOTO = "photo";
const MEDIA_TYPE_VIDEO = "video";

export async function handleSingleMedia(
  ctx: Context,
  downloadRes: DownloadResult,
  filesToDelete: string[],
) {
  const filePath = downloadRes.filePath || "";
  const mediaType = helper.getMediaType(filePath);
  const description = downloadRes.description || undefined;

  switch (mediaType) {
    case MEDIA_TYPE_PHOTO:
      await ctx.replyWithChatAction("upload_photo");
      await ctx.replyWithPhoto(new InputFile(filePath), {
        reply_parameters: { message_id: ctx.message?.message_id || 0 },
        caption: description,
      });
      filesToDelete.push(filePath);
      break;
    case MEDIA_TYPE_VIDEO:
      await ctx.replyWithChatAction("upload_video");
      await ctx.replyWithVideo(new InputFile(filePath), {
        reply_parameters: { message_id: ctx.message?.message_id || 0 },
        caption: description,
      });
      break;
    default:
      log.info(`Unsupported media type: ${downloadRes}`);
  }
}

export async function handleMediaGroup(
  ctx: Context,
  result: DownloadResult[] | null,
  filesToDelete: string[],
) {
  const media: Array<InputMediaPhoto | InputMediaVideo> = [];

  if (!result) {
    return;
  }

  for (let i = 0; i < result.length; i++) {
    const downloadRes = result[i];

    const filePath = downloadRes.filePath || "";
    const mediaType = helper.getMediaType(filePath);

    let description: string | undefined = undefined;
    if (i == result.length - 1) {
      description = downloadRes.description || undefined;
    }

    switch (mediaType) {
      case MEDIA_TYPE_PHOTO:
        await ctx.replyWithChatAction("upload_photo");
        media.push(
          InputMediaBuilder.photo(new InputFile(filePath), {
            caption: description,
          }),
        );
        filesToDelete.push(filePath);
        break;
      case MEDIA_TYPE_VIDEO:
        await ctx.replyWithChatAction("upload_video");
        media.push(InputMediaBuilder.video(new InputFile(filePath), {
          caption: description,
        }));
        filesToDelete.push(filePath);
        break;
      default:
        log.info(`Unsupported media type: ${downloadRes}`);
    }
  }

  if (media.length > 0) {
    await ctx.replyWithMediaGroup(media, {
      reply_parameters: { message_id: ctx.message?.message_id || 0 },
    });
  }
}
