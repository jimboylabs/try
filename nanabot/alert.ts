export type AlertOptions = {
  title: string;
  priority: "urgent" | "default";
  message: string;
};

interface AlertProvider {
  send(options: AlertOptions): void;
}

export class NtfyProvider implements AlertProvider {
  constructor(private url: URL) {
    if (this.url.protocol !== "ntfy:") {
      throw new Error("Invalid URL provided");
    }
  }

  async send(options: AlertOptions) {
    const credentials = `${this.url.username}:${this.url.password}`;
    const encoded = btoa(credentials);
    const stripSlash = (topic: string): string => {
      return topic.replace(/^\//, "");
    };

    await fetch(
      `https://${this.url.hostname}/${stripSlash(this.url.pathname)}`,
      {
        method: "POST",
        headers: {
          "Title": options.title,
          "Priority": options.priority,
          "Authorization": `Basic ${encoded}`,
        },
        body: options.message,
      },
    );
  }
}

export class AlertService {
  constructor(private provider: AlertProvider) {}

  send(options: AlertOptions) {
    this.provider.send(options);
  }
}
