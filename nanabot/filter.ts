export function filterExcludedTerms(filePath: string): boolean {
  const excludedTerms = [
    "logo",
    "gif",
    "collage",
    "iphone",
    "ipad",
    "apple",
    "icon",
  ];

  const excludedPatterns = [
    // Icons and favicons
    /favicon\./i,
    /icons?\./i,
    /.*\.ico$/i,

    // Logo patterns
    /logos?\./i,
    /brand\./i,
    /header-logo/i,
    /footer-logo/i,
    /site-logo/i,

    // Social media icons
    /social-/i,
    /facebook/i,
    /twitter/i,
    /instagram/i,
    /linkedin/i,

    // UI elements
    /buttons?\./i,
    /background\./i,
    /bg-/i,
    /banner\./i,
    /sprites?\./i,
    /nav-/i,
    /menu-/i,

    // Advertisements related
    /ad-/i,
    /ads-/i,
    /advert/i,
    /banner-/i,

    // Common small image indicators
    /thumb/i,
    /avatar/i,
    /profile-pic/i,

    // Commons paths
    /.*\/icons\/.*/i,
    /.*\/ui\/.*/i,

    // Design elements
    /separator/i,
    /divider/i,
    /bullet/i,
    /pixel\./i,
    /spacer\./i,
    /close-/i,
    /search-/i,
    /arrow-/i,
    /scroll-/i,
    /loading/i,
    /header-/i,
    /footer-/i,
    /sidebar-/i,
    /badge/i,
    /ribbon/i,
    /star-/i,
    /rating-/i,
  ];

  const lowercasePath = filePath.toLowerCase();

  return !(
    excludedTerms.some((term) => lowercasePath.includes(term)) ||
    excludedPatterns.some((pattern) => pattern.test(lowercasePath))
  );
}
