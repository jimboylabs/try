#!/usr/bin/env nu
def main [] {}

def "main run" [] {
    deno run --unstable-kv --unstable-cron -A main.ts
}

def "main env:pull" [] {
    infisical export --format=dotenv-export --env=dev > .envrc
}

def "main browserless:run" [] {
   podman container run -p 3000:3000 ghcr.io/browserless/chrome
}

def "main tests:run" [] {
    deno test --allow-env
}
