const IGNORED_DOMAINS = [
  "reddit.com",
  "threads.net",
  "wikipedia.org",
  "gitlab.com",
  "github.com",
  "cs.cmu.edu",
  "laravel.com",
  "bsky.app",
  ".social",
  ".coop",
  "mastodon",
  "hachyderm.io",
];

export function isIgnoredURL(url: string): boolean {
  const parsedURL = new URL(url);
  return IGNORED_DOMAINS.some((domain) => parsedURL.hostname.includes(domain));
}

export function isInstagramPostUrl(url: string): boolean {
  return url.startsWith("https://www.instagram.com/p/");
}

export function isInstagramReelUrl(url: string): boolean {
  return url.startsWith("https://www.instagram.com/reel/");
}

export function isYoutubeUrl(url: string): boolean {
  const youtubePattern = /^(https?:\/\/)?(www\.)?(youtube\.com|youtu\.be)\/.+/;
  return youtubePattern.test(url);
}

export function isValidURL(urlString: string): boolean {
  try {
    new URL(urlString);
    return true;
  } catch {
    return false;
  }
}

export function getMediaType(filePath: string): "photo" | "video" | "unknown" {
  const extension = filePath.toLowerCase().split(".").pop() || "";

  const photoExtensions = ["jpg", "jpeg", "png", "gif", "webp"];
  const videoExtensions = ["mp4", "mov", "avi", "mkv", "webm"];

  if (photoExtensions.includes(extension)) {
    return "photo";
  }

  if (videoExtensions.includes(extension)) {
    return "video";
  }

  return "unknown";
}

export function truncateText(text: string, maxLength: number = 800): string {
  if (text.length <= maxLength) {
    return text;
  }

  return text.substring(0, maxLength) + "...";
}
