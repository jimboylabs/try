import { assertEquals } from "@std/assert";
import { Config, initConfig } from "./config.ts";

Deno.test("initConfig - should return valid config", () => {
  const telegramApiToken = "token123";
  const ntfyUrl = "ntfy://notify.rollingsayu.xyz/topic";
  const playwrightWsEndpoint = "ws://localhost:3000/chrome/playwright";

  Deno.env.set("TELEGRAM_API_TOKEN", telegramApiToken);
  Deno.env.set("NTFY_URL", ntfyUrl);
  Deno.env.set("PLAYWRIGHT_WS_ENDPOINT", playwrightWsEndpoint);

  const result = initConfig();

  const expectedConfig: Config = {
    telegramApiToken,
    ntfyUrl,
    playwrightWsEndpoint,
  };

  assertEquals(result.ok, true);
  if (result.ok) {
    assertEquals(result.value, expectedConfig);
  }
});
