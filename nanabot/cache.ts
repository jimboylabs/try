import { DownloadResult, DownloadStrategy } from "./download.ts";
import { Result } from "./types.ts";
import * as log from "@std/log";

const CACHE_EXPIRE_DAYS = 1;

export class CachedDownloadStrategy implements DownloadStrategy {
  constructor(
    private kv: Deno.Kv,
    private strategy: DownloadStrategy,
  ) {}

  async download(
    url: string,
    downloadFolder: string,
  ): Promise<Result<Array<DownloadResult>>> {
    const cacheKey = ["download_cache", url];
    const cachedResult = await this.kv.get<Array<DownloadResult>>(cacheKey);

    if (cachedResult.value) {
      log.info(`Using cached value of ${cacheKey}`);
      const { value } = cachedResult;
      console.log({ value });
      return { ok: true, value: value };
    }

    const result = await this.strategy.download(url, downloadFolder);
    if (result.ok) {
      await this.kv.set(cacheKey, result, {
        expireIn: CACHE_EXPIRE_DAYS * 24 * 60 * 60 * 1000,
      });

      log.info(`Set cache value of ${cacheKey}`);
    }

    return result;
  }
}
