import { ulid } from "@std/ulid";
import { crypto } from "@std/crypto";
import { chromium } from "playwright-core";
import * as helper from "./helper.ts";
import { filterExcludedTerms } from "./filter.ts";
import { Result } from "./types.ts";

export type DownloadResult = {
  filePath: string;
  description: string | null;
};

export interface DownloadStrategy {
  download(
    url: string,
    downloadFolder: string,
  ): Promise<Result<Array<DownloadResult>>>;
}

export class YouGetStrategy implements DownloadStrategy {
  constructor(private wsEndpoint: string) {}

  private async getOgSite(
    url: string,
    wsEndpoint: string,
  ): Promise<Result<string>> {
    const browser = await chromium.connect(wsEndpoint);
    const page = await browser.newPage();
    await page.goto(url);
    const ogSite = await page.locator('meta[property="og:site_name"]')
      .getAttribute("content");
    await browser.close();

    return { ok: true, value: ogSite || "" };
  }

  private async getTweetContent(
    url: string,
    wsEndpoint: string,
  ): Promise<Result<string>> {
    const browser = await chromium.connect(wsEndpoint);
    const page = await browser.newPage();
    await page.goto(url);
    const tweetText = await page.locator('[data-testid="tweetText"]')
      .textContent();
    return { ok: true, value: tweetText || "" };
  }

  async download(
    url: string,
    downloadFolder: string,
  ): Promise<Result<Array<DownloadResult>>> {
    const result = Array<DownloadResult>();

    // Skip Mastodon URLs
    const ogSiteResult = await this.getOgSite(url, this.wsEndpoint);
    if (
      ogSiteResult.ok &&
      ["mastodon"].includes(ogSiteResult.value.toLowerCase())
    ) {
      return { ok: true, value: result };
    }

    let description: string | null = null;

    const encoder = new TextEncoder();
    const data = encoder.encode(url);
    const hashBuffer = await crypto.subtle.digest("SHA-256", data);
    const hashArray = Array.from(new Uint8Array(hashBuffer));
    const shortHash = hashArray.slice(0, 4).map((b) =>
      b.toString(16).padStart(2, "0")
    ).join("");

    const uniqueFolder = `${downloadFolder}/${shortHash}`;

    if (
      (url.includes("twitter.com") ||
        url.includes("x.com") && this.wsEndpoint != "")
    ) {
      const tweetContentResult = await this.getTweetContent(
        url,
        this.wsEndpoint,
      );
      if (tweetContentResult.ok && tweetContentResult.value != "") {
        description = `${tweetContentResult.value}\n\n${url}`;
      }
    }

    const command = new Deno.Command("you-get", {
      args: ["-o", uniqueFolder, url],
    });

    await command.output();
    const files = Array.from(Deno.readDirSync(uniqueFolder));

    // Avoid downloading too many files
    if (files.length > 2) {
      return { ok: true, value: result };
    }

    if (files.length > 0) {
      const mappedFiles = files
        .map((file) => ({
          filePath: `${uniqueFolder}/${file.name}`,
          description: description ? helper.truncateText(description) : null,
        }))
        .filter((result) => helper.getMediaType(result.filePath) !== "unknown")
        .filter((result) => filterExcludedTerms(result.filePath));

      return { ok: true, value: mappedFiles };
    }

    return { ok: true, value: result };
  }
}

export class InstaPostStrategy implements DownloadStrategy {
  async download(
    url: string,
    downloadFolder: string,
  ): Promise<Result<Array<DownloadResult>>> {
    const result = Array<DownloadResult>();

    if (!helper.isInstagramPostUrl(url)) {
      const err = Error("Invalid Instagram post URL");
      return { ok: false, error: err };
    }

    const shortcode = url.split("/p/")[1]?.split("/")[0];

    const command = new Deno.Command("instaloader", {
      args: [
        "--dirname-pattern",
        `${downloadFolder}/${shortcode}`,
        "--",
        `-${shortcode}`,
      ],
    });

    await command.output();
    const files = Array.from(
      Deno.readDirSync(`${downloadFolder}/${shortcode}`),
    );

    const txtFile = files.find((file) => file.name.endsWith(".txt"));
    let description: string | null = null;
    if (txtFile) {
      description = await Deno.readTextFile(
        `${downloadFolder}/${shortcode}/${txtFile.name}`,
      );
    }

    if (files.length > 0) {
      const mappedFiles = files.map((file) => ({
        filePath: `${downloadFolder}/${shortcode}/${file.name}`,
        description: description ? helper.truncateText(description) : null,
      })).filter((result) =>
        helper.getMediaType(result.filePath) !== "unknown"
      );

      return { ok: true, value: mappedFiles };
    }

    return { ok: true, value: result };
  }
}

export class InstaReelStrategy implements DownloadStrategy {
  async download(
    url: string,
    downloadFolder: string,
  ): Promise<Result<Array<DownloadResult>>> {
    const result = Array<DownloadResult>();

    if (!helper.isInstagramReelUrl(url)) {
      const err = Error("Invalid Instagram reel URL");
      return { ok: false, error: err };
    }

    const shortcode = url.split("/reel/")[1]?.split("/")[0];

    const command = new Deno.Command("instaloader", {
      args: [
        "--dirname-pattern",
        `${downloadFolder}/${shortcode}`,
        "--",
        `-${shortcode}`,
      ],
    });

    await command.output();
    const files = Array.from(
      Deno.readDirSync(`${downloadFolder}/${shortcode}`),
    );

    const downloadRes: DownloadResult[] = [];
    const txtFile = files.find((file) => file.name.endsWith(".txt"));

    if (txtFile) {
      const description = await Deno.readTextFile(
        `${downloadFolder}/${shortcode}/${txtFile.name}`,
      );

      const jpgFile = files.find((file) => file.name.endsWith(".jpg"));
      const mp4File = files.find((file) => file.name.endsWith(".mp4"));

      if (jpgFile) {
        downloadRes.push({
          filePath: `${downloadFolder}/${shortcode}/${jpgFile.name}`,
          description,
        });
      }

      if (mp4File) {
        downloadRes.push({
          filePath: `${downloadFolder}/${shortcode}/${mp4File.name}`,
          description,
        });
      }

      return { ok: true, value: downloadRes };
    }

    return { ok: true, value: result };
  }
}

export class YoutubeStrategy implements DownloadStrategy {
  async download(
    url: string,
    downloadFolder: string,
  ): Promise<Result<Array<DownloadResult>>> {
    const id = ulid();
    const uniqueFolder = `${downloadFolder}/${id}`;
    const command = new Deno.Command("yt-dlp", {
      args: [
        "-o",
        `${uniqueFolder}/${id}.%(ext)s`,
        "--write-description",
        "-f",
        "mp4",
        url,
      ],
    });

    const output = await command.output();

    if (!output.success) {
      const err = Error(`Command failed with status ${output.code}`);
      return { ok: false, error: err };
    }

    const files = Array.from(Deno.readDirSync(uniqueFolder));
    const videoFile = files.find((f) => f.name.endsWith(".mp4"));
    const descFile = files.find((f) => f.name.endsWith(".description"));

    if (!videoFile) {
      const err = Error(`Command failed with status ${output.code}`);
      return { ok: false, error: err };
    }

    const description = descFile
      ? await Deno.readTextFile(`${uniqueFolder}/${descFile.name}`)
      : "Downloaded with yt-dlp";

    return {
      ok: true,
      value: [{
        filePath: `${uniqueFolder}/${videoFile.name}`,
        description: helper.truncateText(description),
      }],
    };
  }
}
