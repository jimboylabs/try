import * as log from "@std/log";
import { AlertService } from "./alert.ts";

export function init(kv: Deno.Kv, alert: AlertService) {
  Deno.cron("Clean Downloads Cache", "0 0 * * SUN", async () => {
    log.info("Running cron job...");
    const now = new Date();
    const nowStr = now.toISOString();
    log.info(`Current time: ${nowStr}`);

    const entries = kv.list({ prefix: ["download_cache"] });
    for await (const entry of entries) {
      log.info(`Deleting cache for ${entry.key}`);
      await kv.delete(entry.key);
    }

    log.info("Delete all files in downloads");

    const deletedPath = Array<string>();

    for await (const entry of Deno.readDir("downloads")) {
      log.info(`Deleting ${entry.name}`);
      const path = `downloads/${entry.name}`;
      await Deno.remove(path, { recursive: true });

      deletedPath.push(path);
    }

    const message = deletedPath.join("\n");

    alert.send({
      title: "(Karuna) Staled cache files was deleted",
      priority: "urgent",
      message: message,
    });

    log.info("Cron job completed.");
  });
}
