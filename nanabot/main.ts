import { Bot, GrammyError, HttpError } from "grammy";
import * as log from "@std/log";
import * as helper from "./helper.ts";
import {
  DownloadResult,
  DownloadStrategy,
  InstaPostStrategy,
  InstaReelStrategy,
  YouGetStrategy,
  YoutubeStrategy,
} from "./download.ts";
import { CachedDownloadStrategy } from "./cache.ts";
import { isRateLimited } from "./ratelimiter.ts";
import { init as initCron } from "./cron.ts";
import * as handler from "./handler.ts";
import { AlertService, NtfyProvider } from "./alert.ts";
import { Config, initConfig } from "./config.ts";
import { Result } from "./types.ts";

const kv = await Deno.openKv();

log.setup({
  handlers: {
    default: new log.ConsoleHandler("INFO", {
      formatter: log.formatters.jsonFormatter,
      useColors: false,
    }),
  },
});

async function tryDownloadStrategies(
  config: Config,
  url: string,
  downloadFolder: string,
): Promise<Result<Array<DownloadResult>>> {
  try {
    // Try YouTube first
    const ytStrategy = new YoutubeStrategy();
    const ytResult = await ytStrategy.download(url, downloadFolder);
    if (ytResult.ok && ytResult.value.length > 0) {
      log.info("Successfully downloaded using YouTube strategy");
      return ytResult;
    }
  } catch (err) {
    log.error("Error while trying YouTube strategy", err);
  }

  // Fallback to YouGet
  log.info("Falling back to YouGet strategy");
  const youGetStrategy = new YouGetStrategy(config.playwrightWsEndpoint);
  const result = await youGetStrategy.download(url, downloadFolder);
  if (result.ok && result.value.length > 0) {
    return result;
  }

  return { ok: true, value: Array<DownloadResult>() };
}

// In the strategy selection:
function getDownloadStrategy(config: Config, url: string): DownloadStrategy {
  switch (true) {
    case helper.isInstagramPostUrl(url): {
      return new CachedDownloadStrategy(kv, new InstaPostStrategy());
    }
    case helper.isInstagramReelUrl(url): {
      return new CachedDownloadStrategy(kv, new InstaReelStrategy());
    }
    default: {
      return new CachedDownloadStrategy(kv, {
        download: (url: string, downloadFolder: string) =>
          tryDownloadStrategies(config, url, downloadFolder),
      });
    }
  }
}

function main() {
  const configResult = initConfig();

  if (!configResult.ok) {
    log.error(configResult.error);
    Deno.exit(1);
  }

  const { value: config } = configResult;

  log.info("Initalizing alert system...");
  const ntfy = new NtfyProvider(new URL(config.ntfyUrl));
  const alert = new AlertService(ntfy);

  log.info("Initializing cron...");
  initCron(kv, alert);

  log.info("Starting bot...");

  const bot = new Bot(config.telegramApiToken);

  const signals = ["SIGINT", "SIGTERM"] as const;
  for (const signal of signals) {
    Deno.addSignalListener(signal, async () => {
      log.info(`Received ${signal}, shutting down...`);
      await bot.stop();
      log.info("Exiting...");
      Deno.exit(0);
    });
  }

  bot.command("start", (ctx) => {
    const helpMessage =
      "H-hmph! You're lucky I'm here to help you download stuff!\n\n" +
      "Listen up, because I'm only saying this once!\n\n" +
      "I can download these things for you (n-not that I want to...):\n\n" +
      "📸 Instagram~\n" +
      "• Posts with multiple photos/videos\n" +
      "• Reels... they're not bad I guess\n\n" +
      "🐦 Twitter!\n" +
      "• GIFs (they're pretty cute...)\n" +
      "• Photos\n" +
      "• Videos, if you're into that sort of thing\n\n" +
      "📺 YouTube!\n" +
      "• Videos... but don't expect me to watch them with you or anything! Baka!\n\n" +
      "J-just send me the link already! It's not like I'm waiting for you or anything...\n\n" +
      "(｡•́︿•̀｡)";

    ctx.reply(helpMessage);
  });

  bot.on("message", async (ctx) => {
    const userId = ctx.from?.id.toString();
    if (!userId) {
      return;
    }

    // Check rate limit
    if (await isRateLimited(userId)) {
      await ctx.reply(
        "H-hey! (｀ε´) You need to wait a minute before asking me again!\n\n" +
          "It's not like I'm trying to avoid you or anything... I just need a break! (｡•́︿•̀｡)\n\n" +
          "Come back in a minute, b-baka! (╯°□°）╯︵ ┻━┻",
      );
      return;
    }

    const text = ctx.message.text?.toString() || "";

    // Extract URL from text
    const urlMatch = text.match(/https?:\/\/[^\s]+/);
    const url = urlMatch ? urlMatch[0] : "";

    if (!helper.isValidURL(url)) {
      log.info(`Invalid URL: ${url}`);
      return;
    }

    if (helper.isIgnoredURL(url)) {
      log.info(`Ignored URL: ${url}`);
      return;
    }

    await ctx.replyWithChatAction("typing");

    log.info(`Downloading media from ${url}`);

    const strategy = getDownloadStrategy(config, url);
    const result = await strategy.download(
      url,
      "./downloads",
    );

    const filesToDelete: string[] = [];

    if (!result.ok) {
      log.error(result.error);
      return;
    }

    if (result.value.length == 1) {
      await handler.handleSingleMedia(ctx, result.value[0], filesToDelete);
    } else {
      await handler.handleMediaGroup(ctx, result.value, filesToDelete);
    }
  });

  bot.catch((err) => {
    const ctx = err.ctx;
    console.error(`Error while handling update ${ctx.update.update_id}:`);
    const e = err.error;
    if (e instanceof GrammyError) {
      console.error("Error in request:", e.description);
    } else if (e instanceof HttpError) {
      console.error("Could not contact Telegram:", e);
    } else {
      console.error("Unknown error:", e);
    }
  });

  bot.start();
}

if (import.meta.main) {
  try {
    main();
  } catch (error) {
    log.error(error);
  }
}
