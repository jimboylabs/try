import { Result } from "./types.ts";

export type Config = {
  telegramApiToken: string;
  ntfyUrl: string;
  playwrightWsEndpoint: string;
};

export function initConfig(): Result<Config> {
  const config: Config = {
    telegramApiToken: Deno.env.get("TELEGRAM_API_TOKEN") || "",
    ntfyUrl: Deno.env.get("NTFY_URL") || "",
    playwrightWsEndpoint: Deno.env.get("PLAYWRIGHT_WS_ENDPOINT") || "",
  };

  if (config.telegramApiToken == "") {
    const err = Error("Invalid telegram api token");
    return { ok: false, error: err };
  }

  if (config.ntfyUrl == "") {
    const err = Error("Invalid ntfy url");
    return { ok: false, error: err };
  }

  if (config.playwrightWsEndpoint == "") {
    const err = Error("Invalid playwright ws endpoint");
    return { ok: false, error: err };
  }

  return { ok: true, value: config };
}
