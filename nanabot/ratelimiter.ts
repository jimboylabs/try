const kv = await Deno.openKv();

export async function isRateLimited(userId: string): Promise<boolean> {
  const now = Date.now();
  const windowStart = now - 60_000; // 1 minute window
  const key = ["ratelimit", userId, now];

  // Get all requests in the last minute
  const requests = kv.list({ prefix: ["ratelimit", userId] });
  let count = 0;

  for await (const entry of requests) {
    if (entry.key[2] as number < windowStart) {
      count++;
    }
  }

  if (count >= 4) {
    return true;
  }

  // Store this request
  await kv.set(key, 1, { expireIn: 60_000 });
  return false;
}
