import { filterExcludedTerms } from "./filter.ts";
import { assertEquals, assertFalse } from "@std/assert";

Deno.test("filterExcludedTerms - should filter icon files", () => {
  const inputs = [
    "scmp-icon-192x192.png",
    "favicon.ico",
    "apple-touch-icon.png",
    "icon-512x512.png",
  ];

  const results = inputs.map(filterExcludedTerms);

  results.forEach((result) => {
    assertFalse(result);
  });
});

Deno.test("filterExcludedTerms - should handle mixed files", () => {
  const inputs = [
    "icon-180x180.png",
    "script.js",
    "manifest.json",
    "favicon-16x16.png",
  ];

  const expected = [false, true, true, false];
  const results = inputs.map(filterExcludedTerms);

  assertEquals(results, expected);
});
