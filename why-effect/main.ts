import { Effect } from "effect";

interface FetchError {
  readonly _tag: "FetchError";
}

interface JsonError {
  readonly _tag: "JsonError";
}

interface SaveError {
  readonly _tag: "SaveError";
}

const fetchRequest = Effect.tryPromise({
  try: () => fetch("https://pokeapi.co/api/v2/pokemon/ditto"),
  catch: (): FetchError => ({ _tag: "FetchError" }),
});

const jsonResponse = (response: Response) =>
  Effect.tryPromise({
    try: () => response.json(),
    catch: (): JsonError => ({ _tag: "JsonError" }),
  });

const savePokemon = (pokemon: unknown) =>
  Effect.tryPromise({
    try: () => fetch("/api/pokemon", { body: JSON.stringify(pokemon) }),
    catch: (): SaveError => ({ _tag: "SaveError" }),
  });

if (import.meta.main) {
  const main = fetchRequest.pipe(
    Effect.flatMap(jsonResponse),
    Effect.flatMap(savePokemon),
    // Effect.catchTag(
    //   "UnknownException",
    //   () => Effect.succeed("There was an error"),
    // ),
  );

  Effect.runPromise(main).then(console.log);
}
