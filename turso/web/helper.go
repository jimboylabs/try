package web

import (
	"encoding/json"
	"fmt"
	"net/http"
)

func Param(r *http.Request, key string) string {
	return r.PathValue(key)
}

func Decode[T any](r *http.Request) (T, error) {
	var val T
	if err := json.NewDecoder(r.Body).Decode(&val); err != nil {
		return val, fmt.Errorf("decode json: %w", err)
	}

	return val, nil
}

func Respond(w http.ResponseWriter, data any, statusCode int) error {
	if statusCode == http.StatusNoContent {
		w.WriteHeader(statusCode)
	}

	jsonData, err := json.Marshal(data)
	if err != nil {
		return err
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)

	if _, err := w.Write(jsonData); err != nil {
		return err
	}

	return nil
}
