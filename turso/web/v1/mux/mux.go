package mux

import (
	"database/sql"
	"net/http"
	"os"

	"gitlab.com/jimboylabs/try/turso/logger"
	"gitlab.com/jimboylabs/try/turso/web"
)

type Options struct {
	corsOrigin []string
}

type Config struct {
	DB       *sql.DB
	Log      *logger.Logger
	Shutdown chan os.Signal
	Build    string
}

type RouteAdder interface {
	Add(app *web.App, cfg Config)
}

func WebAPI(cfg Config, routeAdder RouteAdder, options ...func(opts *Options)) http.Handler {
	var opts Options
	for _, option := range options {
		option(&opts)
	}

	app := web.NewApp(
		cfg.Shutdown,
	)

	if len(opts.corsOrigin) > 0 {
		// TODO: enable cors
	}

	routeAdder.Add(app, cfg)

	return app
}
