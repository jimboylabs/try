package web

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"syscall"

	"github.com/lithammer/shortuuid/v3"
	"gitlab.com/jimboylabs/try/turso/logger"
)

var _ http.Handler = (*App)(nil)

type Handler func(ctx context.Context, w http.ResponseWriter, r *http.Request) error

type App struct {
	mux      *http.ServeMux
	shutdown chan os.Signal
}

func NewApp(shutdown chan os.Signal) *App {
	mux := http.NewServeMux()

	return &App{
		mux:      mux,
		shutdown: shutdown,
	}
}

func (a *App) SignalShutdown() {
	a.shutdown <- syscall.SIGTERM
}

// ServeHTTP implements http.Handler.
func (a *App) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	a.mux.ServeHTTP(w, r)
}

func (a *App) Handle(method string, group string, path string, handler Handler) {
	h := func(w http.ResponseWriter, r *http.Request) {
		correlationID := r.Header.Get("X-Correlation-ID")
		if correlationID == "" {
			correlationID = "gen_" + shortuuid.New()
		}

		ctx := logger.ContextWithCorrelationID(r.Context(), correlationID)

		_ = handler(ctx, w, r)
	}

	finalPath := path
	if group != "" {
		finalPath = "/" + group + path
	}
	finalPath = fmt.Sprintf("%s %s", method, finalPath)

	a.mux.HandleFunc(finalPath, h)
}
