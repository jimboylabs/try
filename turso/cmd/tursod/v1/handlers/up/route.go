package up

import (
	"database/sql"
	"net/http"

	"gitlab.com/jimboylabs/try/turso/logger"
	"gitlab.com/jimboylabs/try/turso/web"
)

type Config struct {
	Log   *logger.Logger
	DB    *sql.DB
	Build string
}

func Routes(app *web.App, cfg Config) {
	h := new(cfg.Build, cfg.Log, cfg.DB)
	app.Handle(http.MethodGet, "apis", "/up", h.liveness)
}
