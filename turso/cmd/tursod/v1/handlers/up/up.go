package up

import (
	"context"
	"database/sql"
	"net/http"
	"os"
	"runtime"

	"gitlab.com/jimboylabs/try/turso/logger"
	"gitlab.com/jimboylabs/try/turso/web"
)

type handlers struct {
	log   *logger.Logger
	db    *sql.DB
	build string
}

func new(build string, log *logger.Logger, db *sql.DB) *handlers {
	return &handlers{
		build: build,
		log:   log,
		db:    db,
	}
}

func (h *handlers) liveness(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	host, err := os.Hostname()
	if err != nil {
		host = "unavailable"
	}

	data := struct {
		Status     string `json:"status,omitempty"`
		Build      string `json:"build,omitempty"`
		Host       string `json:"host,omitempty"`
		GOMAXPROCS int    `json:"GOMAXPROCS,omitempty"`
	}{
		Status:     "up",
		Build:      h.build,
		Host:       host,
		GOMAXPROCS: runtime.GOMAXPROCS(0),
	}

	return web.Respond(w, data, http.StatusOK)
}
