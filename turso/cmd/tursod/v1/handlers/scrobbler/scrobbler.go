package scrobbler

import (
	"database/sql"
	"net/http"

	"gitlab.com/jimboylabs/try/turso/logger"
	"gitlab.com/jimboylabs/try/turso/web"
	"golang.org/x/net/context"
)

type handlers struct {
	log   *logger.Logger
	db    *sql.DB
	build string
}

func new(build string, log *logger.Logger, db *sql.DB) *handlers {
	return &handlers{
		log:   log,
		db:    db,
		build: build,
	}
}

func (h *handlers) create(ctx context.Context, w http.ResponseWriter, r *http.Request) error {
	return web.Respond(w, nil, http.StatusOK)
}
