package v1

import (
	"gitlab.com/jimboylabs/try/turso/cmd/tursod/v1/handlers/scrobbler"
	"gitlab.com/jimboylabs/try/turso/cmd/tursod/v1/handlers/up"
	"gitlab.com/jimboylabs/try/turso/web"
	"gitlab.com/jimboylabs/try/turso/web/v1/mux"
)

func Routes() add {
	return add{}
}

var _ mux.RouteAdder = (*add)(nil)

type add struct{}

// Add implements mux.RouteAdder.
func (add) Add(app *web.App, cfg mux.Config) {
	up.Routes(app, up.Config{
		Log:   cfg.Log,
		DB:    cfg.DB,
		Build: cfg.Build,
	})

	scrobbler.Routes(app, scrobbler.Config{
		Log:   cfg.Log,
		DB:    cfg.DB,
		Build: cfg.Build,
	})
}
