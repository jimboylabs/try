package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/kelseyhightower/envconfig"
	"github.com/lithammer/shortuuid/v3"
	v1 "gitlab.com/jimboylabs/try/turso/cmd/tursod/v1"
	"gitlab.com/jimboylabs/try/turso/data/sql"
	"gitlab.com/jimboylabs/try/turso/data/sqldb"
	"gitlab.com/jimboylabs/try/turso/logger"
	"gitlab.com/jimboylabs/try/turso/web/v1/mux"
)

var build = "develop"

func main() {
	var log *logger.Logger

	events := logger.Events{
		Error: func(ctx context.Context, _ logger.Record) {
			log.Info(ctx, "******* SEND ALERT ******")
		},
	}

	correlationIDFn := func(ctx context.Context) string {
		return logger.CorrelationIDFromContext(ctx)
	}

	log = logger.NewWithEvents(os.Stdout, logger.LevelInfo, "tursod", correlationIDFn, events)

	correlationID := shortuuid.New()
	ctx := logger.ContextWithCorrelationID(context.Background(), correlationID)

	if err := run(ctx, log); err != nil {
		log.Error(ctx, "startup", "msg", err)
		os.Exit(1)
	}
}

func run(ctx context.Context, log *logger.Logger) error {
	log.Info(ctx, "startup", "msg", "loading env config")

	cfg := struct {
		Web struct {
			ShutdownTimeout time.Duration `envconfig:"api_shutdown_timeout" default:"20s"`
			APIHost         string        `envconfig:"api_host" default:"0.0.0.0:3000"`
		}
		LibsqlRunMigrations bool   `envconfig:"libsql_run_migrations" default:"false"`
		LibsqlUrl           string `envconfig:"libsql_url"`
		LibsqlAuthToken     string `envconfig:"libsql_auth_token"`
	}{}

	err := envconfig.Process("", &cfg)
	if err != nil {
		return err
	}

	log.Info(ctx, "startup", "msg", "env config loaded!")

	log.Info(ctx, "startup", "status", "initializing database support", "url", cfg.LibsqlUrl)

	db, err := sqldb.MustOpen(sqldb.Config{
		Url:       cfg.LibsqlUrl,
		AuthToken: cfg.LibsqlAuthToken,
	})
	if err != nil {
		return err
	}
	defer func() {
		log.Info(ctx, "shutdown", "status", "stopping database support", "url", cfg.LibsqlUrl)
		db.Close()
	}()

	if cfg.LibsqlRunMigrations {
		log.Info(ctx, "startup", "status", "running db migrations")
		err := sql.Migrate(ctx, db)
		if err != nil {
			return err
		}

		log.Info(ctx, "startup", "status", "db migrations executed")
	}

	// -------------------------------------------------------------------------
	// Start API Service

	log.Info(ctx, "startup", "status", "initializing API support")

	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, syscall.SIGINT, syscall.SIGTERM)

	cfgMux := mux.Config{
		Build:    build,
		Shutdown: shutdown,
		Log:      log,
		DB:       db,
	}

	api := http.Server{
		Addr:     cfg.Web.APIHost,
		Handler:  mux.WebAPI(cfgMux, v1.Routes()),
		ErrorLog: logger.NewStdLogger(log, logger.LevelError),
	}

	serverErrors := make(chan error, 1)

	go func() {
		log.Info(ctx, "startup", "status", "api router started", "host", api.Addr)

		serverErrors <- api.ListenAndServe()
	}()

	// -------------------------------------------------------------------------
	// Shutdown

	select {
	case err := <-serverErrors:
		return fmt.Errorf("server error: %w", err)
	case sig := <-shutdown:
		log.Info(ctx, "shutdown", "status", "shutdown started", "signal", sig)
		defer log.Info(ctx, "shutdown", "status")

		ctx, cancel := context.WithTimeout(ctx, cfg.Web.ShutdownTimeout)
		defer cancel()

		if err := api.Shutdown(ctx); err != nil {
			api.Close()
			return fmt.Errorf("could not stop server gracefully: %w", err)
		}
	}

	return nil
}
