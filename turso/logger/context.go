package logger

import (
	"context"
	"os"
)

type ctxKey int

const (
	loggerKey        ctxKey = iota
	correlationIDKey ctxKey = iota
)

func FromContext(ctx context.Context) *Logger {
	log, ok := ctx.Value(loggerKey).(*Logger)
	if ok {
		return log
	}

	correlationIDFn := func(ctx context.Context) string {
		return CorrelationIDFromContext(ctx)
	}

	return New(os.Stdout, LevelInfo, "tursod", correlationIDFn)
}

func ToContext(ctx context.Context, log *Logger) context.Context {
	return context.WithValue(ctx, loggerKey, log)
}
