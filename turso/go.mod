module gitlab.com/jimboylabs/try/turso

go 1.22.0

require (
	github.com/ThreeDotsLabs/watermill v1.3.5
	github.com/ThreeDotsLabs/watermill-redisstream v1.2.2
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/lithammer/shortuuid/v3 v3.0.7
	github.com/redis/go-redis/v9 v9.5.1
	github.com/tursodatabase/libsql-client-go v0.0.0-20240220085343-4ae0eb9d0898
)

require (
	github.com/Rican7/retry v0.3.1 // indirect
	github.com/antlr/antlr4/runtime/Go/antlr/v4 v4.0.0-20230512164433-5d1fd1a340c9 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/uuid v1.3.1 // indirect
	github.com/klauspost/compress v1.15.15 // indirect
	github.com/libsql/sqlite-antlr4-parser v0.0.0-20230802215326-5cb5bb604475 // indirect
	github.com/oklog/ulid v1.3.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/vmihailenco/msgpack v4.0.4+incompatible // indirect
	golang.org/x/exp v0.0.0-20220722155223-a9213eeb770e // indirect
	golang.org/x/net v0.5.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
	nhooyr.io/websocket v1.8.7 // indirect
)
