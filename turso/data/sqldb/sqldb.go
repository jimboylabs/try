package sqldb

import (
	"database/sql"
	"errors"
	"net/url"

	_ "github.com/tursodatabase/libsql-client-go/libsql"
)

type Config struct {
	Url       string
	AuthToken string
}

func MustOpen(cfg Config) (*sql.DB, error) {
	base := cfg.Url

	if cfg.AuthToken == "" {
		panic(errors.New("auth token is required"))
	}

	params := url.Values{}
	params.Add("authToken", cfg.AuthToken)

	dburl := base + "?" + params.Encode()

	db, err := sql.Open("libsql", dburl)
	if err != nil {
		panic(err)
	}

	return db, nil
}
