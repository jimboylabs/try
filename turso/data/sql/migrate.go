package sql

import (
	"context"
	"database/sql"
	_ "embed"
)

//go:embed init.sql
var initSQL string

func Migrate(ctx context.Context, db *sql.DB) error {
	_, err := db.ExecContext(ctx, initSQL)
	if err != nil {
		return err
	}

	return nil
}
