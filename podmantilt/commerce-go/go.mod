module commerce_go

go 1.22.7

toolchain go1.22.10

require (
	github.com/99designs/gqlgen v0.17.57
	github.com/amacneil/dbmate/v2 v2.23.0
	github.com/caarlos0/env/v6 v6.10.1
	github.com/go-chi/chi/v5 v5.1.0
	github.com/joho/godotenv v1.5.1
	github.com/vektah/gqlparser/v2 v2.5.19
)

require (
	filippo.io/edwards25519 v1.1.0 // indirect
	github.com/agnivade/levenshtein v1.1.1 // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.5 // indirect
	github.com/go-chi/cors v1.2.1 // indirect
	github.com/go-sql-driver/mysql v1.8.1 // indirect
	github.com/go-viper/mapstructure/v2 v2.2.1 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/hashicorp/golang-lru/v2 v2.0.7 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/sosodev/duration v1.3.1 // indirect
	github.com/urfave/cli/v2 v2.27.5 // indirect
	github.com/xrash/smetrics v0.0.0-20240521201337-686a1a2994c1 // indirect
	golang.org/x/mod v0.22.0 // indirect
	golang.org/x/sync v0.9.0 // indirect
	golang.org/x/text v0.20.0 // indirect
	golang.org/x/tools v0.27.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
