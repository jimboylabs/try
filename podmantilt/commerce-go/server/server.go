package server

import (
	"commerce_go/config"
	"commerce_go/graph/generated"
	"commerce_go/graph/resolver"
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/cors"
)

type server struct {
	cfg    *config.Config
	doneCh chan struct{}
}

func NewServer(cfg *config.Config) *server {
	return &server{cfg: cfg, doneCh: make(chan struct{})}
}

func (server *server) Run(parentCtx context.Context) error {
	_, cancel := signal.NotifyContext(parentCtx, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)
	defer cancel()

	r := chi.NewRouter()
	r.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"https://*", "http://*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: false,
		MaxAge:           300,
	}))

	gqlResolver := resolver.Resolver{}

	gqlServer := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &gqlResolver}))

	r.Get("/playground", playground.Handler("GraphQL playground", "/graphql"))
	r.Handle("/graphql", gqlServer)

	api := &http.Server{
		Addr:    fmt.Sprintf(":%s", server.cfg.ApiPort),
		Handler: r,
	}

	log.Printf("Web server started at port %s", api.Addr)

	api.ListenAndServe()

	<-server.doneCh

	log.Printf("Application %s exited properly", "commerce_go")

	return nil
}
