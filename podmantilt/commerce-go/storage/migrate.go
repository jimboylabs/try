package storage

import (
	"commerce_go/config"
	"embed"
	"fmt"
	"net/url"

	"github.com/amacneil/dbmate/v2/pkg/dbmate"
	_ "github.com/amacneil/dbmate/v2/pkg/driver/mysql"
)

//go:embed migrations/*.sql
var migrationFileFS embed.FS

func Migrate(cfg *config.Config) error {
	if !cfg.Storage.RunMigration {
		return nil
	}

	u, err := url.Parse(cfg.Storage.Mysql.DatabaseURL)
	if err != nil {
		return err
	}

	db := dbmate.New(u)
	db.MigrationsDir = []string{"./migrations/"}
	db.FS = migrationFileFS

	migrations, err := db.FindMigrations()
	if err != nil {
		panic(err)
	}

	for _, m := range migrations {
		fmt.Println(m.Version, m.FilePath)
	}

	err = db.CreateAndMigrate()
	if err != nil {
		panic(err)
	}

	return nil
}
