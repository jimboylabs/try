package config

import (
	"log"

	"github.com/caarlos0/env/v6"
	"github.com/joho/godotenv"
)

type Config struct {
	ApiPort string `env:"PORT" envDefault:"8000"`

	Storage struct {
		RunMigration bool `env:"DATABASE_RUN_MIGRATION" envDefault:"false"`
		Mysql        struct {
			DatabaseURL string `env:"DATABASE_URL"`
		}
	}
}

func InitConfig() (*Config, error) {
	if err := godotenv.Load(); err != nil {
		log.Print("Error loading .env file")
	}

	cfg := Config{}
	if err := env.Parse(&cfg); err != nil {
		log.Fatalf("%+v", err)
	}

	return &cfg, nil
}
