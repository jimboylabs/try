package main

import (
	"commerce_go/config"
	"commerce_go/server"
	"commerce_go/storage"
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	cfg, err := config.InitConfig()
	if err != nil {
		log.Fatal(err)
	}

	if cfg.Storage.RunMigration {
		err := storage.Migrate(cfg)
		if err != nil {
			log.Fatal(err)
		}
	}

	ctx, cancel := context.WithCancel(context.Background())

	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, os.Interrupt, syscall.SIGTERM)
	go func() {
		select {
		case <-shutdown:
			log.Println("Interrupt signal received. Shutting down...")
			cancel()
		case <-ctx.Done():
			log.Println("Context canceled. Shutting down...")
		}
	}()

	errChan := make(chan error, 1)
	go func() {
		errChan <- server.NewServer(cfg).Run(ctx)
	}()

	// Wait for server to exit or context to be canceled
	select {
	case err := <-errChan:
		log.Fatalf("Server error: %v", err)
	case <-ctx.Done():
		// do nothing
	}
}
