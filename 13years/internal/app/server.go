package app

import (
	"13years/pkg/logger"
	"context"
	"fmt"
	"io"
	"net"
	"net/http"
	"time"
)

type Config struct {
	Host string
	Port string
}

func NewConfig(getenv func(string) string) *Config {
	return &Config{
		Host: getenv("HOST"),
		Port: getenv("PORT"),
	}
}

func NewServer(logger *logger.Logger, config *Config) http.Handler {
	mux := http.NewServeMux()
	addRoutes(mux)

	var handler http.Handler = mux
	handler = loggingMiddleware(logger, handler)

	return handler
}

func Run(
	ctx context.Context,
	args []string,
	getenv func(string) string,
	stdin io.Reader,
	stdout, stderr io.Writer,
) error {
	config := NewConfig(getenv)

	logger := logger.NewLogger(stdout, stderr)

	srv := NewServer(
		logger,
		config,
	)
	httpServer := &http.Server{
		Addr:    net.JoinHostPort(config.Host, config.Port),
		Handler: srv,
	}

	go func() {
		logger.Info(ctx, "listening on %s\n", httpServer.Addr)
		if err := httpServer.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			fmt.Fprintf(stderr, "error listening and serving: %s\n", err)
		}
	}()

	<-ctx.Done()

	ctxShutdown, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := httpServer.Shutdown(ctxShutdown); err != nil {
		return fmt.Errorf("server shutdown failed: %w", err)
	}

	logger.Info(ctx, "server exited properly")

	return nil
}
