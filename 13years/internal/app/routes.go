package app

import "net/http"

func addRoutes(mux *http.ServeMux) {
	mux.Handle("/api", handleAPI())
	mux.Handle("/about", handleAbout())
	mux.Handle("/", handleIndex())
	mux.Handle("/admin", adminOnly(handleAdminIndex()))
}
