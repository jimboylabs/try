package app

import "net/http"

func handleAPI() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		encode(w, r, http.StatusOK, map[string]any{"message": "Hello API!"})
	})
}

func handleAbout() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		encode(w, r, http.StatusOK, map[string]any{"message": "About"})
	})
}

func handleIndex() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		encode(w, r, http.StatusOK, map[string]any{"message": "Index"})
	})
}

func handleAdminIndex() http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		encode(w, r, http.StatusOK, map[string]any{"message": "Hello Admin!"})
	})
}
