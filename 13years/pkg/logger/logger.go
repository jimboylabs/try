package logger

import (
	"context"
	"io"
	"log"
)

type Logger struct {
	infoLog  *log.Logger
	errorLog *log.Logger
}

func NewLogger(out, err io.Writer) *Logger {
	return &Logger{
		infoLog:  log.New(out, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile),
		errorLog: log.New(err, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile),
	}
}

func (l *Logger) Info(ctx context.Context, msg string, keysAndValues ...any) {
	l.infoLog.Printf("%s %v", msg, keysAndValues)
}

func (l *Logger) Error(ctx context.Context, msg string, keysAndValues ...any) {
	l.errorLog.Printf("%s %v", msg, keysAndValues)
}
