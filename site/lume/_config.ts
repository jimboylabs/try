import lume from "lume/mod.ts";
import jsx from "lume/plugins/jsx_preact.ts";
import mdx from "lume/plugins/mdx.ts";
import tailwindcss from "lume/plugins/tailwindcss.ts";
import postcss from "lume/plugins/postcss.ts";
import esbuild from "lume/plugins/esbuild.ts";

const site = lume({
  src: "./src",
  dest: "../backend/_sites",
});

site.use(jsx());
site.use(mdx());
site.use(tailwindcss({
  extensions: [".mdx", ".tsx", ".html", ".md", ".jsx"],
}));
site.use(postcss());
site.use(esbuild({
  extensions: [".jsx"],
  options: {
    bundle: true,
    format: "esm",
    minify: true,
    keepNames: true,
    platform: "browser",
    target: "esnext",
    treeShaking: true,
  },
}));

export default site;
