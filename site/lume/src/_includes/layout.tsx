export default ({ title, children }: Lume.Data) => (
  <html>
    <head>
      <link rel="stylesheet" href="/styles.css"></link>
      <title>{title} - jimmy's blog</title>
    </head>
    <body>
      {children}
    </body>
  </html>
);
