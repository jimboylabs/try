import { useEffect } from "npm:preact/hooks";

export default function () {
  useEffect(() => {
    console.log("Hello World");
  }, []);

  return <button type="button" onClick={() => {
    console.log("Hello World");
  }}>Greeting</button>;
}
