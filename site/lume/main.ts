import { GreetService } from "./gen/greet/v1/greet_connect.ts";
import { createConnectTransport } from "npm:@connectrpc/connect-web";
import { createCallbackClient } from "npm:@connectrpc/connect";

// This transport is going to be used throughout the app
const transport = createConnectTransport({
  baseUrl: "http://localhost:8080",
});

const client = createCallbackClient(GreetService, transport);

client.greet({}, (err, res) => {
  if (!err) {
    console.log(res);
  }
});
