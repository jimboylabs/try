package main

import (
	"context"
	"log"
	"net/http"
	greetv1 "site/gen/greet/v1"
	"site/gen/greet/v1/greetv1connect"

	"connectrpc.com/connect"
)

var _ greetv1connect.GreetServiceHandler = (*GreetServer)(nil)

type GreetServer struct{}

// Greet implements greetv1connect.GreetServiceHandler.
func (g *GreetServer) Greet(ctx context.Context, req *connect.Request[greetv1.GreetRequest]) (*connect.Response[greetv1.GreetResponse], error) {
	log.Println("Request headers:", req.Header())
	res := connect.NewResponse(&greetv1.GreetResponse{
		Greeting: "Hello World",
	})
	res.Header().Set("Greet-Version", "v1")
	return res, nil
}

func main() {
	greeter := &GreetServer{}
	mux := http.NewServeMux()
	path, handler := greetv1connect.NewGreetServiceHandler(greeter)
	mux.Handle(path, handler)
	http.ListenAndServe(":8080", mux)
}
