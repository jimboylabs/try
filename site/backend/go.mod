module site

go 1.22.0

require (
	connectrpc.com/connect v1.15.0
	google.golang.org/protobuf v1.33.0
)
