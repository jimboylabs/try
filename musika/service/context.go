package service

import (
	"context"
	"time"

	"github.com/lithammer/shortuuid/v3"
)

type ctxKey int

const key ctxKey = 1

type Values struct {
	CorrelationID string
	Now           time.Time
	StatusCode    int
}

func GetValues(ctx context.Context) *Values {
	v, ok := ctx.Value(key).(*Values)
	if !ok {
		return &Values{
			CorrelationID: "gen_" + shortuuid.New(),
			Now:           time.Now(),
		}
	}

	return v
}

func GetCorrelationID(ctx context.Context) string {
	v, ok := ctx.Value(key).(*Values)
	if !ok {
		return "gen_" + shortuuid.New()
	}

	return v.CorrelationID
}

func SetCorrelationID(ctx context.Context, correlationID string) context.Context {
	values := GetValues(ctx)
	values.CorrelationID = correlationID

	return context.WithValue(ctx, key, values)
}
