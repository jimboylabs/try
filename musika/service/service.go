package service

import (
	"context"
	"errors"
	"musika/config"
	"musika/logger"
	"net/http"
	"unicode/utf8"

	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/lithammer/shortuuid/v3"
	"go.temporal.io/sdk/client"
	"golang.org/x/sync/errgroup"
)

type Service struct {
	config        *config.Config
	log           *logger.Logger
	echoFramework *echo.Echo
}

func New(config *config.Config, db *sqlx.DB, log *logger.Logger, temporalClient client.Client) Service {
	e := echo.New()
	e.HideBanner = true

	e.Use(
		middleware.RequestIDWithConfig(middleware.RequestIDConfig{
			Generator: func() string {
				return shortuuid.New()
			},
		}),
		middleware.BodyDump(func(c echo.Context, reqBody, resBody []byte) {
			reqID := c.Response().Header().Get(echo.HeaderXRequestID)

			var reqBodyStr = string(reqBody)
			var resBodyStr = "<binary data>"

			if utf8.ValidString(string(resBody)) {
				resBodyStr = string(resBody)
			}

			log.Info(c.Request().Context(), "Request/Response", "request_id", reqID, "request", reqBodyStr, "response", resBodyStr)
		}),
		middleware.RequestLoggerWithConfig(middleware.RequestLoggerConfig{
			LogStatus:   true,
			LogURI:      true,
			LogError:    true,
			HandleError: true,
			LogValuesFunc: func(c echo.Context, v middleware.RequestLoggerValues) error {
				ctx := c.Request().Context()

				if v.Error == nil {
					log.Info(ctx, "REQUEST", "uri", v.URI, "status", v.Status)
				} else {
					log.Error(ctx, "REQUEST_ERROR", "uri", v.URI, "status", v.Status, "err", v.Error.Error())
				}

				return nil
			},
		}),
		func(next echo.HandlerFunc) echo.HandlerFunc {
			return func(c echo.Context) error {
				req := c.Request()
				ctx := req.Context()

				correlationID := req.Header.Get("Corrrelation-ID")
				if correlationID == "" {
					correlationID = "gen_" + shortuuid.New()
				}

				ctx = SetCorrelationID(ctx, correlationID)

				c.SetRequest(req.WithContext(ctx))
				c.Response().Header().Set("Corrrelation-ID", correlationID)

				return next(c)
			}
		},
	)

	e.HTTPErrorHandler = HandleError(log)

	SetupEchoRoutes(e, config, db, log, temporalClient)

	return Service{
		config:        config,
		log:           log,
		echoFramework: e,
	}
}

func (s Service) Run(ctx context.Context) error {
	s.log.Info(ctx, "startup", "status", "initializing V1 API Support")

	errgrp, ctx := errgroup.WithContext(ctx)

	srv := http.Server{
		Addr:         s.config.Web.APIHost,
		Handler:      s.echoFramework,
		WriteTimeout: s.config.Web.WriteTimeout,
		IdleTimeout:  s.config.Web.IdleTimeout,
		ErrorLog:     logger.NewStdLogger(s.log, logger.LevelError),
	}

	errgrp.Go(func() error {
		s.log.Info(ctx, "startup", "status", "api router started", "host", s.config.Web.APIHost)

		err := srv.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			return err
		}

		return nil
	})

	errgrp.Go(func() error {
		<-ctx.Done()

		s.log.Info(ctx, "shutdown", "status", "shutdown started")
		defer s.log.Info(ctx, "shutdown", "status", "shutdown complete")

		return srv.Close()
	})

	return errgrp.Wait()
}

func HandleError(log *logger.Logger) func(error, echo.Context) {
	return func(err error, c echo.Context) {
		if c.Response().Committed {
			return
		}

		log.Error(c.Request().Context(), "HTTP_ERROR", "error", err.Error())

		httpCode := http.StatusInternalServerError
		msg := any("Internal Server Error")

		httpErr := &echo.HTTPError{}
		if errors.As(err, &httpErr) {
			httpCode = httpErr.Code
			msg = httpErr.Message
		}

		jsonErr := c.JSON(httpCode, map[string]any{
			"error": msg,
		})
		if jsonErr != nil {
			panic(err)
		}
	}
}
