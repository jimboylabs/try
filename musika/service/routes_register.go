package service

import (
	"musika/config"
	"musika/database/sqldb"
	"musika/database/transaction"
	"musika/logger"
	"musika/module/scrobbler"
	"musika/workflow"
	"net/http"

	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo/v4"
	"go.temporal.io/sdk/client"
)

func SetupEchoRoutes(e *echo.Echo, config *config.Config, db *sqlx.DB, log *logger.Logger, temporalClient client.Client) {
	apis := e.Group("/apis/mlj_1")

	apis.GET("/health_check", func(c echo.Context) error {
		return c.JSON(http.StatusOK, map[string]string{
			"msg": "ok",
		})
	})

	apis.POST("/newscrobble", func(c echo.Context) error {
		beginner := sqldb.NewBeginner(db)
		scrobbleRepo := scrobbler.NewRepository(db, log)

		scrobbleUsecase := scrobbler.NewUsecase(config, log, scrobbleRepo)

		type newScrobbleRequest struct {
			Artist string `json:"artist"`
			Title  string `json:"title"`
		}

		ctx := c.Request().Context()
		req := new(newScrobbleRequest)

		if err := c.Bind(&req); err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}

		var createScrobbler = &scrobbler.Scrobbler{
			TrackTitle: req.Title,
			ArtistName: req.Artist,
		}

		err := transaction.ExecuteUnderTransaction(ctx, log, beginner, func(tx transaction.Transaction) error {
			scrobbleUsecase, err := scrobbleUsecase.ExecuteUnderTransaction(tx)
			if err != nil {
				return err
			}

			return scrobbleUsecase.Create(ctx, createScrobbler)
		})
		if err != nil {
			log.Error(ctx, "error", err.Error())
			return echo.NewHTTPError(http.StatusInternalServerError, "Something went wrong.")
		}

		_, err = temporalClient.ExecuteWorkflow(ctx, client.StartWorkflowOptions{
			TaskQueue: config.Temporal.QueueName,
		}, workflow.PopulateWorkflow, createScrobbler.ID)
		if err != nil {
			log.Error(ctx, "Failed executing workflow", err.Error())
		}

		return c.JSON(http.StatusOK, map[string]any{
			"msg": "ok",
		})
	})
}
