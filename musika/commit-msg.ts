/**
 * Generates a Git commit message based on the code diff provided.
 * The commit message is limited to 80 characters and summarizes the changes in the code diff.
 */

const command = new Deno.Command("git", {
  args: [
    "diff",
    "--staged",
  ],
});

// Generates a git commit message for a code diff that summarizes the changes in one sentence.
const { code: _code, stdout, stderr: _stderr } = await command.output();

const diff = new TextDecoder().decode(stdout);

/**
 * Generates a git commit message based on a code diff.
 * The commit message is limited to 80 characters and summarizes the changes in the code diff.
 */
const prompt = `
Generate a git commit message written in present tense for the following code diff with the following specification:
1. The message must be a maximum 80 characters.
2. Summarize the code, what the code does, in one sentence.
3. Language must be English.
4. IMPORTANT! Please respond with ONLY the commit message. EXCLUDE everything else!

Code diff:
${diff}
`;
const response = await fetch(
  `${Deno.env.get("OLLAMA_BASE_URL")}/api/generate`,
  {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      model: "llama3",
      prompt,
      stream: false,
    }),
  },
);

const data = await response.json();

console.log(data.response);

export {};

