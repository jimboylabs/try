package main

import (
	"context"
	"fmt"
	"musika/config"
	"musika/database/migrate"
	"musika/database/sqldb"
	"musika/logger"
	"musika/service"
	"os"
	"os/signal"
	"runtime"

	"go.temporal.io/sdk/client"
)

var build = "develop"

func main() {
	var log *logger.Logger

	events := logger.Events{
		Error: func(ctx context.Context, r logger.Record) {
			log.Info(ctx, "******* SEND ALERT ******")
		},
	}

	correlationIDFn := func(ctx context.Context) string {
		return service.GetCorrelationID(ctx)
	}

	log = logger.NewWithEvents(os.Stdout, logger.LevelDebug, "MUSIKA-API", correlationIDFn, events)

	// -------------------------------------------------------------------------

	ctx := context.Background()

	if err := run(ctx, log); err != nil {
		log.Error(ctx, "startup", "error", err.Error())
		os.Exit(1)
	}
}

func run(ctx context.Context, log *logger.Logger) error {
	// -------------------------------------------------------------------------
	// GOMAXPROCS

	log.Info(ctx, "startup", "GOMAXPROCS", runtime.GOMAXPROCS(0))

	// -------------------------------------------------------------------------
	// App Starting

	log.Info(ctx, "starting service", "version", build)
	defer log.Info(ctx, "shutdown complete")

	cfg, err := config.New()
	if err != nil {
		log.Error(ctx, "CONFIG_ERROR", err.Error())
	}

	// -------------------------------------------------------------------------
	// Temporal Support

	log.Info(ctx, "startup", "status", "initializing temporal support")

	temporalClient, err := client.Dial(client.Options{
		Namespace: cfg.Temporal.Namespace,
		HostPort:  cfg.Temporal.Host,
		Logger:    logger.NewSlogLogger(log),
	})
	if err != nil {
		return fmt.Errorf("temporal client dial: %w", err)
	}
	defer func() {
		log.Info(ctx, "shutdown", "status", "stopping temporal support")
		temporalClient.Close()
	}()

	// -------------------------------------------------------------------------
	// Database Support

	log.Info(ctx, "startup", "status", "initializing database support")

	db, err := sqldb.Open(cfg)
	if err != nil {
		return fmt.Errorf("connecting to db: %w", err)
	}

	defer func() {
		log.Info(ctx, "shutdown", "status", "stopping database support")
		db.Close()
	}()

	err = migrate.RunMigrations(ctx, cfg, log)
	if err != nil {
		log.Error(ctx, "Database migration failed", "error", err.Error())
	}

	// -------------------------------------------------------------------------
	// Start API Service

	ctx, cancel := signal.NotifyContext(ctx, os.Interrupt)
	defer cancel()

	return service.New(cfg, db, log, temporalClient).Run(ctx)
}
