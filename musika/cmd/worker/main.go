package main

import (
	"context"
	"fmt"
	"musika/cachestore"
	"musika/client/spotify"
	"musika/config"
	"musika/database/sqldb"
	"musika/logger"
	"musika/module/scrobbler"
	"musika/service"
	"musika/workflow"
	"musika/workflow/activity"
	"os"
	"time"

	"github.com/redis/go-redis/v9"
	"go.temporal.io/sdk/client"
	"go.temporal.io/sdk/worker"
)

func main() {
	var log *logger.Logger

	events := logger.Events{
		Error: func(ctx context.Context, r logger.Record) {
			log.Info(ctx, "******* SEND ALERT ******")
		},
	}

	correlationIDFn := func(ctx context.Context) string {
		return service.GetCorrelationID(ctx)
	}

	ctx := context.Background()

	log = logger.NewWithEvents(os.Stdout, logger.LevelDebug, "MUSIKA-API", correlationIDFn, events)
	if err := run(ctx, log); err != nil {
		log.Error(ctx, "startup", "error", err.Error())
		os.Exit(1)
	}
}

func run(ctx context.Context, log *logger.Logger) error {
	cfg, err := config.New()
	if err != nil {
		return err
	}

	// -------------------------------------------------------------------------
	// Temporal Support

	log.Info(ctx, "startup", "status", "initializing temporal support")

	client, err := client.Dial(client.Options{
		Namespace: cfg.Temporal.Namespace,
		HostPort:  cfg.Temporal.Host,
		Logger:    logger.NewSlogLogger(log),
	})
	if err != nil {
		return fmt.Errorf("temporal client dial: %w", err)
	}

	defer func() {
		log.Info(ctx, "shutdown", "status", "stopping temporal support")
		client.Close()
	}()

	// -------------------------------------------------------------------------
	// Database Support

	log.Info(ctx, "startup", "status", "initializing database support")

	db, err := sqldb.Open(cfg)
	if err != nil {
		return fmt.Errorf("connecting to db: %w", err)
	}

	defer func() {
		log.Info(ctx, "shutdown", "status", "stopping database support")
		db.Close()
	}()

	log.Info(ctx, "startup", "status", "initializing redis")

	redisClient := redis.NewClient(&redis.Options{
		Addr:     cfg.Redis.Addr,
		Password: cfg.Redis.Password,
	})

	// TODO: consider fallback to in memory cachestore
	// redis connection was failed
	_, err = redisClient.Ping(ctx).Result()
	if err != nil {
		return fmt.Errorf("connecting to redis: %w", err)
	}

	defer func() {
		log.Info(ctx, "shutdown", "status", "stopping redis connection")
		redisClient.Close()
	}()

	spotifyCacheStore, err := cachestore.NewRedisCacheStore(redisClient, 60*time.Minute, cfg.Redis.Prefix)
	if err != nil {
		return fmt.Errorf("creating spotify cachestore: %w", err)
	}

	spotifyClient := spotify.New(cfg, log, spotifyCacheStore)

	scrobblerRepo := scrobbler.NewRepository(db, log)

	searchTrackActivity := activity.SearchTrackActivity{
		SpotifyClient: spotifyClient,
		Repo:          scrobblerRepo,
	}

	w := worker.New(client, cfg.Temporal.QueueName, worker.Options{})
	w.RegisterActivity(searchTrackActivity.Execute)
	w.RegisterWorkflow(workflow.PopulateWorkflow)

	return w.Run(worker.InterruptCh())
}
