package spotify

import (
	"context"
	"fmt"
	"musika/cachestore"
	"musika/config"
	"musika/logger"
	"net/url"

	"github.com/go-resty/resty/v2"
)

var _ Client = (*client)(nil)

var baseURL = "https://api.spotify.com/v1"

type Client interface {
	Login(ctx context.Context) (string, error)
	SearchArtists(ctx context.Context, query string) (SearchResponse, error)
	SearchTracks(ctx context.Context, query string) (SearchResponse, error)
}

type client struct {
	resty      *resty.Client
	config     *config.Config
	log        *logger.Logger
	cachestore cachestore.CacheStore
}

// Login implements Client.
func (c *client) Login(ctx context.Context) (string, error) {
	existing, err := c.cachestore.Get(ctx, "spotify", "token")
	if err != nil {
		return "", err
	}
	if existing != "" {
		return existing, nil
	}

	body := map[string]string{
		"grant_type":    "client_credentials",
		"client_id":     c.config.Spotify.ClientID,
		"client_secret": c.config.Spotify.ClientSecret,
	}

	var response LoginResponse

	_, err = c.resty.R().
		SetFormData(body).
		SetResult(&response).
		Post("https://accounts.spotify.com/api/token")
	if err != nil {
		return "", err
	}

	if err := c.cachestore.Set(ctx, "spotify", "token", response.AccessToken); err != nil {
		return "", err
	}

	return response.AccessToken, nil
}

// SearchArtists implements Client.
func (c *client) search(ctx context.Context, query string, kind string) (SearchResponse, error) {
	var searchResponse SearchResponse

	token, err := c.Login(ctx)
	if err != nil {
		return searchResponse, err
	}

	restyC := c.resty.R()

	if token != "" {
		restyC.SetHeader("Authorization", fmt.Sprintf("Bearer %s", token))
	} else {
		c.log.Debug(ctx, "response access is empty")
	}

	u, err := url.Parse(fmt.Sprintf("%s/search", baseURL))
	if err != nil {
		return searchResponse, err
	}

	q := u.Query()
	q.Set("q", query)
	q.Set("type", kind)
	u.RawQuery = q.Encode()

	_, err = restyC.
		SetResult(&searchResponse).
		Get(u.String())
	if err != nil {
		return searchResponse, err
	}

	return searchResponse, nil
}

// SearchArtists implements Client.
func (c *client) SearchArtists(ctx context.Context, query string) (SearchResponse, error) {
	return c.search(ctx, query, "artist,track")
}

// SearchTracks implements Client.
func (c *client) SearchTracks(ctx context.Context, query string) (SearchResponse, error) {
	return c.search(ctx, query, "artist,track")
}

func New(config *config.Config, log *logger.Logger, cs cachestore.CacheStore) Client {
	return &client{
		resty:      resty.New().SetDebug(config.Spotify.Debug),
		config:     config,
		log:        log,
		cachestore: cs,
	}
}
