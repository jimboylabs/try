package track

import (
	"context"
	"errors"
	"musika/config"
	"musika/database/sqldb"
	"musika/database/transaction"
	"musika/logger"
)

var _ Usecase = (*usecase)(nil)

type Usecase interface {
	ExecuteUnderTransaction(tx transaction.Transaction) (Usecase, error)
	Create(ctx context.Context, track *Track) error
	FindByTitle(ctx context.Context, title string) (track *Track, err error)
}

type usecase struct {
	repo Repository
	log  *logger.Logger
}

func NewUsecase(config *config.Config, log *logger.Logger, repo Repository) Usecase {
	return &usecase{
		repo: repo,
		log:  log,
	}
}

func (u *usecase) ExecuteUnderTransaction(tx transaction.Transaction) (Usecase, error) {
	repo, err := u.repo.ExecuteUnderTransaction(tx)
	if err != nil {
		return nil, err
	}

	return &usecase{
		log:  u.log,
		repo: repo,
	}, nil
}

// Create implements Usecase.
func (u *usecase) Create(ctx context.Context, track *Track) error {
	return u.repo.Create(ctx, track)
}

// FindByTitle implements Usecase.
func (u *usecase) FindByTitle(ctx context.Context, title string) (track *Track, err error) {
	findTrack, err := u.repo.FindByTitle(ctx, title)
	if err == nil {
		return findTrack, nil
	}

	if errors.Is(err, sqldb.ErrRecordNotFound) {
		createTrack := &Track{
			Title: title,
		}
		err := u.repo.Create(ctx, createTrack)
		if err != nil {
			return nil, err
		}

		return createTrack, nil
	}

	return
}
