package track

type Track struct {
	ID    int64
	Title string
}
