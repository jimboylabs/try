package track

import (
	"context"
	"musika/database/sqldb"
	"musika/database/transaction"
	"musika/logger"

	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
)

var _ Repository = (*repository)(nil)

type Repository interface {
	ExecuteUnderTransaction(tx transaction.Transaction) (Repository, error)
	Create(ctx context.Context, track *Track) error
	FindByTitle(ctx context.Context, title string) (track *Track, err error)
}

type repository struct {
	db  sqlx.ExtContext
	log *logger.Logger
}

func NewRepository(db *sqlx.DB, log *logger.Logger) Repository {
	return &repository{
		db:  db,
		log: log,
	}
}

// ExecuteUnderTransaction implements Repository.
func (r *repository) ExecuteUnderTransaction(tx transaction.Transaction) (Repository, error) {
	ec, err := sqldb.GetExtContext(tx)
	if err != nil {
		return nil, err
	}

	return &repository{
		db:  ec,
		log: r.log,
	}, nil
}

// Create implements Repository.
func (r *repository) Create(ctx context.Context, track *Track) error {
	query, args, err := sq.Insert("tracks").
		Columns("title").
		Values(track.Title).
		ToSql()
	if err != nil {
		return err
	}

	res, err := sqldb.ExecContext(ctx, r.db, r.log, query, args...)
	if err != nil {
		return err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return err
	}

	track.ID = id

	return nil
}

// FindByTitle implements Repository.
func (r *repository) FindByTitle(ctx context.Context, title string) (track *Track, err error) {
	query, args, err := sq.Select("id", "title").
		From("tracks").
		Where(sq.Eq{"title": title}).
		Limit(1).ToSql()
	if err != nil {
		return nil, err
	}

	row := struct {
		ID    int64  `db:"id"`
		Title string `db:"title"`
	}{}

	err = sqldb.GetContext(ctx, r.db, r.log, &row, query, args...)
	if err != nil {
		return nil, err
	}

	return &Track{
		ID:    row.ID,
		Title: row.Title,
	}, nil
}
