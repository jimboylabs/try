package artist

import (
	"context"
	"musika/database/sqldb"
	"musika/database/transaction"
	"musika/logger"

	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
)

var _ Repository = (*repository)(nil)

type Repository interface {
	ExecuteUnderTransaction(tx transaction.Transaction) (Repository, error)
	Create(ctx context.Context, artist *Artist) error
	FindByName(ctx context.Context, name string) (artist *Artist, err error)
}

type repository struct {
	db  sqlx.ExtContext
	log *logger.Logger
}

func NewRepository(db *sqlx.DB, log *logger.Logger) Repository {
	return &repository{
		db:  db,
		log: log,
	}
}

// FindByName implements Repository.
func (r *repository) FindByName(ctx context.Context, name string) (artist *Artist, err error) {
	query, args, err := sq.Select("id", "name").
		From("artists").
		Where(sq.Eq{"name": name}).
		Limit(1).
		ToSql()
	if err != nil {
		return
	}

	row := struct {
		Id   int64  `db:"id"`
		Name string `db:"name"`
	}{}

	err = sqldb.GetContext(ctx, r.db, r.log, &row, query, args...)
	if err != nil {
		return
	}

	artist = &Artist{
		ID:   row.Id,
		Name: row.Name,
	}

	return
}

// Create implements Repository.
func (r *repository) Create(ctx context.Context, artist *Artist) error {
	query, args, err := sq.Insert("artists").
		Columns("name").
		Values(artist.Name).
		ToSql()
	if err != nil {
		return err
	}

	res, err := sqldb.ExecContext(ctx, r.db, r.log, query, args...)
	if err != nil {
		return err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return err
	}

	artist.ID = id

	return nil
}

// ExecuteUnderTransaction implements Repository.
func (r *repository) ExecuteUnderTransaction(tx transaction.Transaction) (Repository, error) {
	ec, err := sqldb.GetExtContext(tx)
	if err != nil {
		return nil, err
	}

	return &repository{
		db:  ec,
		log: r.log,
	}, nil
}
