package artist

import (
	"context"
	"errors"
	"musika/config"
	"musika/database/sqldb"
	"musika/database/transaction"
	"musika/logger"
)

var _ Usecase = (*usecase)(nil)

type Usecase interface {
	ExecuteUnderTransaction(tx transaction.Transaction) (Usecase, error)
	Create(ctx context.Context, artist *Artist) error
	FindByName(ctx context.Context, name string) (artist *Artist, err error)
}

type usecase struct {
	log  *logger.Logger
	repo Repository
}

func NewUsecase(config *config.Config, log *logger.Logger, repo Repository) Usecase {
	return &usecase{
		log:  log,
		repo: repo,
	}
}

// ExecuteUnderTransaction implements Usecase.
func (u *usecase) ExecuteUnderTransaction(tx transaction.Transaction) (Usecase, error) {
	repo, err := u.repo.ExecuteUnderTransaction(tx)
	if err != nil {
		return nil, err
	}

	return &usecase{
		log:  u.log,
		repo: repo,
	}, nil
}

// FindByName implements Usecase.
func (u *usecase) FindByName(ctx context.Context, name string) (artist *Artist, err error) {
	findArtist, err := u.repo.FindByName(ctx, name)
	if err == nil {
		return findArtist, err
	}

	if errors.Is(err, sqldb.ErrRecordNotFound) {
		createArtist := &Artist{
			Name: name,
		}
		err := u.repo.Create(ctx, createArtist)
		if err != nil {
			return nil, err
		}

		return createArtist, nil
	}

	return
}

// Create implements Usecase.
func (u *usecase) Create(ctx context.Context, artist *Artist) error {
	return u.repo.Create(ctx, artist)
}
