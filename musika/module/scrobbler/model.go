package scrobbler

import "time"

type Scrobbler struct {
	ID       int64
	TrackID  int64
	ArtistID int64

	Album      string
	TrackTitle string
	ArtistName string

	CreatedAt time.Time

	SpotifyData any
}
