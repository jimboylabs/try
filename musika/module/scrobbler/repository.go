package scrobbler

import (
	"context"
	"database/sql"
	"encoding/json"
	"musika/database/sqldb"
	"musika/database/transaction"
	"musika/logger"
	"time"

	sq "github.com/Masterminds/squirrel"
	"github.com/jmoiron/sqlx"
)

var _ Repository = (*repository)(nil)

type Repository interface {
	ExecuteUnderTransaction(tx transaction.Transaction) (Repository, error)
	Create(ctx context.Context, scrobble *Scrobbler) error
	FindByID(ctx context.Context, id int64) (*Scrobbler, error)
	Update(ctx context.Context, id int64, scrobble *Scrobbler) error
}

type repository struct {
	db  sqlx.ExtContext
	log *logger.Logger
}

func NewRepository(db *sqlx.DB, log *logger.Logger) Repository {
	return &repository{
		db:  db,
		log: log,
	}
}

// ExecuteUnderTransaction implements Repository.
func (r *repository) ExecuteUnderTransaction(tx transaction.Transaction) (Repository, error) {
	ec, err := sqldb.GetExtContext(tx)
	if err != nil {
		return nil, err
	}

	return &repository{
		db:  ec,
		log: r.log,
	}, nil
}

// FindByID implements Repository.
func (r *repository) FindByID(ctx context.Context, id int64) (*Scrobbler, error) {
	query, args, err := sq.Select(
		"id",
		"track_id",
		"album",
		"track_title",
		"artist_name",
		"created_at",
	).
		From("scrobblers").
		Where(sq.Eq{"id": id}).
		Limit(1).
		ToSql()
	if err != nil {
		return nil, err
	}

	var row = struct {
		ID         int64          `db:"id"`
		TrackID    sql.NullInt64  `db:"track_id"`
		Album      sql.NullString `db:"album"`
		TrackTitle sql.NullString `db:"track_title"`
		ArtistName sql.NullString `db:"artist_name"`
		CreatedAt  time.Time      `db:"created_at"`
	}{}

	err = sqldb.GetContext(ctx, r.db, r.log, &row, query, args...)
	if err != nil {
		return nil, err
	}

	return &Scrobbler{
		ID:         row.ID,
		TrackID:    row.TrackID.Int64,
		Album:      row.Album.String,
		TrackTitle: row.TrackTitle.String,
		ArtistName: row.ArtistName.String,
		CreatedAt:  row.CreatedAt,
	}, nil
}

// Create implements Repository.
func (r *repository) Create(ctx context.Context, scrobbler *Scrobbler) error {
	if scrobbler.CreatedAt.IsZero() {
		scrobbler.CreatedAt = time.Now()
	}

	query, args, err := sq.Insert("scrobblers").
		Columns(
			"track_title",
			"artist_name",
			"created_at",
		).
		Values(
			scrobbler.TrackTitle,
			scrobbler.ArtistName,
			scrobbler.CreatedAt,
		).
		ToSql()
	if err != nil {
		return err
	}

	res, err := sqldb.ExecContext(ctx, r.db, r.log, query, args...)
	if err != nil {
		return err
	}

	id, err := res.LastInsertId()
	if err != nil {
		return err
	}

	scrobbler.ID = id

	return nil
}

// Update implements Repository.
func (r *repository) Update(ctx context.Context, id int64, scrobble *Scrobbler) error {
	// convert spotify data struct to json string
	var spotifyData string

	if scrobble.SpotifyData != nil {
		b, err := json.Marshal(scrobble.SpotifyData)
		if err != nil {
			return err
		}

		spotifyData = string(b)
	}

	query, args, err := sq.Update("scrobblers").
		Set("spotify_data", spotifyData).
		Where(sq.Eq{"id": id}).
		ToSql()
	if err != nil {
		return err
	}

	_, err = sqldb.ExecContext(ctx, r.db, r.log, query, args...)
	if err != nil {
		return err
	}

	return nil
}
