package scrobbler

import (
	"context"
	"musika/config"
	"musika/database/transaction"
	"musika/logger"
)

var _ Usecase = (*usecase)(nil)

type Usecase interface {
	ExecuteUnderTransaction(tx transaction.Transaction) (Usecase, error)
	Create(ctx context.Context, scrobbler *Scrobbler) error
}

type usecase struct {
	config        *config.Config
	log           *logger.Logger
	scrobblerRepo Repository
}

// ExecuteUnderTransaction implements Usecase.
func (u *usecase) ExecuteUnderTransaction(tx transaction.Transaction) (Usecase, error) {
	repo, err := u.scrobblerRepo.ExecuteUnderTransaction(tx)
	if err != nil {
		return nil, err
	}

	return &usecase{
		log:           u.log,
		scrobblerRepo: repo,
	}, nil
}

// Create implements Usecase.
func (u *usecase) Create(ctx context.Context, scrobbler *Scrobbler) error {
	// create scrobble
	err := u.scrobblerRepo.Create(ctx, scrobbler)
	if err != nil {
		return err
	}

	return nil
}

func NewUsecase(config *config.Config, log *logger.Logger, scrobbleRepo Repository) Usecase {
	return &usecase{
		config:        config,
		log:           log,
		scrobblerRepo: scrobbleRepo,
	}
}
