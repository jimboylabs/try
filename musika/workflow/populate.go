package workflow

import (
	"errors"
	"musika/workflow/activity"
	"time"

	"go.temporal.io/sdk/workflow"
)

func PopulateWorkflow(ctx workflow.Context, scrobblerID int64) error {
	logger := workflow.GetLogger(ctx)

	if scrobblerID == 0 {
		return errors.New("invalid scrobbler id")
	}

	var searchTrackActivity *activity.SearchTrackActivity

	searchTrackRequest := &activity.SearchTrackRequest{ScrobblerID: scrobblerID}

	activityCtx := workflow.WithActivityOptions(ctx, workflow.ActivityOptions{
		StartToCloseTimeout: time.Minute * 5,
	})

	logger.Debug("start search tracks activity")

	var searchTrackResponse activity.SearchTrackResponse

	err := workflow.
		ExecuteActivity(activityCtx, searchTrackActivity.Execute, searchTrackRequest).
		Get(activityCtx, &searchTrackResponse)
	if err != nil {
		return err
	}

	if searchTrackResponse.SpotifyTrackID == "" {
		return errors.New("no tracks found")
	}

	if searchTrackResponse.SpotifyArtistID == "" {
		return errors.New("no artist found")
	}

	return nil
}
