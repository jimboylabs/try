package activity

import (
	"context"
	"fmt"
	"musika/client/spotify"
	"musika/module/scrobbler"
	"strings"

	"go.temporal.io/sdk/activity"
)

type SearchTrackActivity struct {
	SpotifyClient spotify.Client
	Repo          scrobbler.Repository
}

type SearchTrackRequest struct {
	ScrobblerID int64
}

type SearchTrackResponse struct {
	SpotifyTrackID  string
	SpotifyArtistID string
}

func (a *SearchTrackActivity) Execute(ctx context.Context, request *SearchTrackRequest) (*SearchTrackResponse, error) {
	logger := activity.GetLogger(ctx)

	scrobb, err := a.Repo.FindByID(ctx, request.ScrobblerID)
	if err != nil {
		logger.Error("scrobb not found", "request", request)
		return nil, err
	}

	if strings.Contains(scrobb.ArtistName, ",") {
		logger.Debug("multiple artists detected", "artist_names", scrobb.ArtistName)
		artists := strings.Split(scrobb.ArtistName, ",")

		if len(artists) > 0 {
			// Take the first artist name
			scrobb.ArtistName = artists[0]
		}
	}

	query := fmt.Sprintf("%s artist:%s", scrobb.TrackTitle, scrobb.ArtistName)

	logger.Debug("prepare query for searching tracks", "q", query)

	metadata, err := a.SpotifyClient.SearchTracks(ctx, query)
	if err != nil {
		logger.Error("error searching for track metadata", "error", err.Error())
		return nil, err
	}

	res := new(SearchTrackResponse)

	if len(metadata.Tracks.Items) > 0 {
		track := metadata.Tracks.Items[0]
		res.SpotifyTrackID = track.ID

		if len(track.Artists) > 0 {
			artist := track.Artists[0]
			res.SpotifyArtistID = artist.ID
		}

		// update scrobbler's spotify data
		spotifyData := metadata.Tracks
		scrobb.SpotifyData = spotifyData
		err := a.Repo.Update(ctx, scrobb.ID, scrobb)
		if err != nil {
			return nil, err
		}

		logger.Debug("scrobbler successfully updated with spotify data", "id", scrobb.ID)
	}

	logger.Debug(
		"debug spotify track and artist id",
		"spotify_track_id",
		res.SpotifyTrackID,
		"spotify_artist_id",
		res.SpotifyArtistID,
	)

	return res, nil
}
