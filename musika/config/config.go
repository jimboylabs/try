package config

import (
	"time"

	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
)

type WebConfig struct {
	APIHost         string        `envconfig:"api_host" default:"0.0.0.0:3000"`
	WriteTimeout    time.Duration `envconfig:"write_timeout" default:"10s"`
	IdleTimeout     time.Duration `envconfig:"idle_timeout" default:"120s"`
	ShutdownTimeout time.Duration `envconfig:"shutdown_timeout" default:"20s"`
}

type RedisConfig struct {
	Addr     string `envconfig:"addr" default:"localhost:6379"`
	Password string `envconfig:"password" default:""`
	Prefix   string `envconfig:"prefix" default:"develop"`
}

type TemporalConfig struct {
	Namespace string `envconfig:"namespace" default:"develop_musika"`
	QueueName string `envconfig:"queue_name" default:"musika_worker"`
	Host      string `envconfig:"host" default:"localhost:7233"`
}

type SpotifyConfig struct {
	ClientID     string `envconfig:"client_id"`
	ClientSecret string `envconfig:"client_secret"`
	Debug        bool   `envconfig:"debug" default:"false"`
}

type Config struct {
	Web         WebConfig      `envconfig:"web"`
	DatabaseURL string         `envconfig:"database_url" default:"mysql://root:password@localhost:3306/musika?parseTime=true"`
	Spotify     SpotifyConfig  `envconfig:"spotify"`
	Temporal    TemporalConfig `envconfig:"temporal"`
	Redis       RedisConfig    `envconfig:"redis"`
}

func New() (*Config, error) {
	_ = godotenv.Load()

	var config Config
	err := envconfig.Process("", &config)
	if err != nil {
		return nil, err
	}

	return &config, nil
}
