package cachestore

import (
	"context"
	"fmt"
	"time"

	"github.com/go-redis/cache/v9"
	"github.com/redis/go-redis/v9"
)

var _ CacheStore = (*RedisCacheStore)(nil)

type RedisCacheStore struct {
	keyPrefix string
	Data      *cache.Cache
	TTL       time.Duration
}

func NewRedisCacheStore(redisClient *redis.Client, ttl time.Duration, keyPrefix string) (*RedisCacheStore, error) {
	data := cache.New(&cache.Options{
		Redis:      redisClient,
		LocalCache: cache.NewTinyLFU(10_000, ttl),
	})
	return &RedisCacheStore{
		keyPrefix: keyPrefix,
		Data:      data,
		TTL:       ttl,
	}, nil
}

func redisCacheKey(prefix, name, key string) string {
	return fmt.Sprintf("%s_musika/%s/%s", prefix, name, key)
}

// Get implements CacheStore.
func (r *RedisCacheStore) Get(ctx context.Context, name string, key string) (string, error) {
	var val string
	err := r.Data.Get(ctx, redisCacheKey(r.keyPrefix, name, key), &val)
	if err == cache.ErrCacheMiss {
		return "", nil
	}
	if err != nil {
		return "", err
	}
	return val, nil
}

// Purge implements CacheStore.
func (r *RedisCacheStore) Purge(ctx context.Context, name string, key string) error {
	err := r.Data.Delete(ctx, redisCacheKey(r.keyPrefix, name, key))
	if err == cache.ErrCacheMiss {
		return nil
	}
	return err
}

// Set implements CacheStore.
func (r *RedisCacheStore) Set(ctx context.Context, name string, key string, val string) error {
	return r.Data.Set(&cache.Item{
		Ctx:   ctx,
		Key:   redisCacheKey(r.keyPrefix, name, key),
		Value: val,
		TTL:   r.TTL,
	})
}
