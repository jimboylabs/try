package transaction

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"musika/logger"
)

type Transaction interface {
	Commit() error
	Rollback() error
}

type Beginner interface {
	Begin() (Transaction, error)
}

func ExecuteUnderTransaction(ctx context.Context, log *logger.Logger, bgn Beginner, fn func(tx Transaction) error) error {
	hasCommitted := false

	log.Info(ctx, "BEGIN TRANSACTION")
	tx, err := bgn.Begin()
	if err != nil {
		return err
	}

	defer func() {
		if !hasCommitted {
			log.Info(ctx, "ROLLBACK TRANSACTION")
		}

		if err := tx.Rollback(); err != nil {
			if errors.Is(err, sql.ErrTxDone) {
				return
			}

			log.Info(ctx, "ROLLBACK TRANSACTION", "ERROR", err)
		}
	}()

	if err := fn(tx); err != nil {
		return fmt.Errorf("EXECUTE TRANSACTION: %w", err)
	}

	log.Info(ctx, "COMMIT TRANSACTION")
	if err := tx.Commit(); err != nil {
		return fmt.Errorf("COMMIT TRANSACTION: %w", err)
	}

	hasCommitted = true

	return nil
}
