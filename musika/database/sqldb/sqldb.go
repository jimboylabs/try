// Package sqldb provides support for access the database.
package sqldb

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"musika/config"
	"musika/logger"
	"strings"

	"github.com/xo/dburl"

	"github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

const (
	uniqueViolation = uint16(1062)
	tableNotFound   = uint16(1146)
)

var (
	// ErrRecordNotFound is returned when a record is not found.
	ErrRecordNotFound = sql.ErrNoRows

	// ErrDuplicateKey is a constraint violation error.
	// https://dev.mysql.com/doc/mysql-errors/8.0/en/server-error-reference.html#error_er_dup_entry
	ErrDuplicateKey = errors.New("duplicate key value violates table constraint")
)

// Open knows how to open a database connection based on the configuration.
func Open(config *config.Config) (*sqlx.DB, error) {
	db, err := dburl.Open(config.DatabaseURL)
	if err != nil {
		return nil, err
	}

	return sqlx.NewDb(db, "mysql"), err
}

// queryString provides a pretty print version of the query and parameters.
func queryString(query string, params ...any) string {
	for _, param := range params {
		var value string
		switch v := param.(type) {
		case string:
			value = fmt.Sprintf("'%s'", v)
		case []byte:
			value = fmt.Sprintf("'%s'", string(v))
		default:
			value = fmt.Sprintf("%v", v)
		}
		query = strings.Replace(query, "?", value, 1)
	}

	query = strings.ReplaceAll(query, "\t", "")
	query = strings.ReplaceAll(query, "\n", " ")

	return strings.Trim(query, " ")
}

// ExecContext is a helper function to execute CRUD operation, without returning any rows.
func ExecContext(ctx context.Context, db sqlx.ExtContext, log *logger.Logger, query string, args ...any) (res sql.Result, err error) {
	res, err = db.ExecContext(ctx, query, args...)
	if err != nil {
		q := queryString(query, args...)
		log.Error(ctx, "DB_QUERY", "query", q, "error", err)

		var myErr *mysql.MySQLError
		if errors.As(err, &myErr) {
			if myErr.Number == uniqueViolation {
				return res, ErrDuplicateKey
			}
		}
	}

	return
}

// GetContext is a helper to execute query that returns a single row.
func GetContext(ctx context.Context, db sqlx.ExtContext, log *logger.Logger, dest any, query string, args ...any) (err error) {
	err = sqlx.GetContext(ctx, db, dest, query, args...)
	if err != nil {
		q := queryString(query, args...)
		log.Error(ctx, "DB_QUERY", "query", q, "error", err)

		if err == sql.ErrNoRows {
			return ErrRecordNotFound
		}
	}

	return
}

// SelectContext is a helper to execute query that returns multiple rows.
func SelectContext(ctx context.Context, db sqlx.ExtContext, log *logger.Logger, dest any, query string, args ...any) (err error) {
	err = sqlx.SelectContext(ctx, db, dest, query, args...)
	if err != nil {
		q := queryString(query, args...)

		log.Error(ctx, "DB_QUERY", "query", q, "error", err)
	}

	return
}
