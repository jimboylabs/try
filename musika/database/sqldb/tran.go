package sqldb

import (
	"fmt"
	"musika/database/transaction"

	"github.com/jmoiron/sqlx"
)

var _ transaction.Beginner = (*dbBeginner)(nil)

type dbBeginner struct {
	sqlxDB *sqlx.DB
}

// Begin implements transaction.Beginner.
func (d *dbBeginner) Begin() (transaction.Transaction, error) {
	return d.sqlxDB.Beginx()
}

func NewBeginner(sqlxDB *sqlx.DB) transaction.Beginner {
	return &dbBeginner{
		sqlxDB: sqlxDB,
	}
}

func GetExtContext(tx transaction.Transaction) (sqlx.ExtContext, error) {
	ec, ok := tx.(sqlx.ExtContext)
	if !ok {
		return nil, fmt.Errorf("Transactor(%T) not of a type *sql.Tx", tx)
	}

	return ec, nil
}
