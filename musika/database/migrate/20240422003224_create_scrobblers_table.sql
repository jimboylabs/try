-- migrate:up
CREATE TABLE IF NOT EXISTS `scrobblers` (
    id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    track_id BIGINT NULL REFERENCES tracks(id),
    album VARCHAR(255) NULL,
    track_title VARCHAR(255) NULL,
    artist_name VARCHAR(255) NULL,
    created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NULL ON UPDATE CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP NULL,
    CONSTRAINT `fk_scrobblers_track_id` FOREIGN KEY (track_id) REFERENCES tracks(id) ON DELETE CASCADE ON UPDATE CASCADE
);

-- migrate:down
DROP TABLE IF EXISTS `scrobblers`;