-- migrate:up
ALTER TABLE `scrobblers` ADD spotify_data JSON NULL;

-- migrate:down
ALTER TABLE `scrobblers` DROP COLUMN spotify_data;
