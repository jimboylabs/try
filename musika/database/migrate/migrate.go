package migrate

import (
	"context"
	"embed"
	"fmt"
	"musika/config"
	"musika/logger"
	"net/url"

	"github.com/amacneil/dbmate/v2/pkg/dbmate"
	_ "github.com/amacneil/dbmate/v2/pkg/driver/mysql"
)

//go:embed *.sql
var migrationSQLs embed.FS

func RunMigrations(ctx context.Context, config *config.Config, log *logger.Logger) (err error) {
	dbURL, err := url.Parse(config.DatabaseURL)
	if err != nil {
		return err
	}

	hostPort := fmt.Sprintf("%s:%s", dbURL.Hostname(), dbURL.Port())

	log.Info(ctx, "startup", "status", "starting database migration", "hostport", hostPort)
	defer func() {
		if err != nil {
			log.Info(ctx, "startup", "status", "finished database migration", "hostport", hostPort)
		}
	}()

	db := dbmate.New(dbURL)

	db.MigrationsDir = []string{"."}
	db.FS = migrationSQLs

	migrations, err := db.FindMigrations()
	if err != nil {
		return err
	}

	for _, m := range migrations {
		log.Info(ctx, "DB_MIGRATIONS", "version", m.Version, "filepath", m.FilePath)
	}

	return db.CreateAndMigrate()
}
