// @deno-types="@types/react"
import { StrictMode } from "react";
// @deno-types="@types/react-dom/client"
import { createRoot } from "react-dom/client";

import { initKaplay } from "./kaplay.ts";

const k = initKaplay();

k.loadSprite("spritesheet", "./spritesheet.png", {
  sliceX: 39,
  sliceY: 31,
});

createRoot(document.getElementById("root") as HTMLElement).render(
  <StrictMode>
  </StrictMode>,
);
