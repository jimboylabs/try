import kaplay, { KAPLAYCtx } from "kaplay";

export function initKaplay(): KAPLAYCtx {
  const canvas = document.getElementById("game") as HTMLCanvasElement;
  return kaplay({
    global: false,
    touchToMouse: true,
    canvas: canvas,
  });
}
