import { NativeConnection, Worker } from "@temporalio/worker";
import * as activities from "./activities.ts";

async function run() {
  const connection = await NativeConnection.connect({
    address: "localhost:7233",
  });

  try {
    const worker = await Worker.create({
      connection,
      namespace: "default",
      taskQueue: "hello-world",
      workflowsPath: "./workflows.ts",
      activities,
    });
    await worker.run();
  } finally {
    await connection.close();
  }
}

if (import.meta.main) {
  run().catch((err) => {
    console.error(err);
    Deno.exit(1);
  });
}
