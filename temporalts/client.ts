import { Client, Connection } from "@temporalio/client";
import { example } from "./workflows.ts";
import { ulid } from "@std/ulid";

async function run() {
  const connection = await Connection.connect({ address: "localhost:7233" });

  const client = new Client({
    connection,
  });

  const handle = await client.workflow.start(example, {
    taskQueue: "hello-world",
    args: ["Temporal"],
    workflowId: `workflow-${ulid()}`,
  });
  console.log(`Started workflow with runId ${handle.workflowId}`);

  console.log(await handle.result());
}

if (import.meta.main) {
  run().catch((err) => {
    console.error(err);
    Deno.exit(1);
  });
}
