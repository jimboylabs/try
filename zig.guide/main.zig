const std = @import("std");

const contant: i32 = 5;
var variable: u32 = 5000;

const a = [5]u8{ 'h', 'e', 'l', 'l', 'o' };
const b = [_]u8{ 'w', 'o', 'r', 'l', 'd' };

const length = a.len;

pub fn main() void {
    std.debug.print("Hello, {s}!\n", .{"World"});
    std.debug.print("Length: {d}.", .{length});
}
