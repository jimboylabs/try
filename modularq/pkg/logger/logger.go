package logger

import (
	"io"
	"log"
)

type Logger struct {
	infoLog  *log.Logger
	errorLog *log.Logger
}

func NewLogger(out, err io.Writer) *Logger {
	return &Logger{
		infoLog:  log.New(out, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile),
		errorLog: log.New(out, "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile),
	}
}

func (l *Logger) Info(msg string) {
	l.infoLog.Print(msg)
}

func (l *Logger) Infof(msg string, args ...any) {
	l.infoLog.Printf(msg, args...)
}

func (l *Logger) Error(msg string) {
	l.errorLog.Print(msg)
}

func (l *Logger) Errorf(msg string, args ...any) {
	l.errorLog.Printf(msg, args...)
}

func (l *Logger) Fatalf(msg string, args ...any) {
	l.errorLog.Fatalf(msg, args...)
}
