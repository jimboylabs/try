package db

type DB interface {
	Close() error
}
