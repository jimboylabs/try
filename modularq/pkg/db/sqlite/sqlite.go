package sqlite

import (
	"context"
	"database/sql"
	"modularq/pkg/db"
	"time"

	"github.com/charmbracelet/log"
	"modernc.org/sqlite"
	sqlitelib "modernc.org/sqlite/lib"
)

var _ db.DB = (*DB)(nil)

const (
	DbName    = "modularq_sqlite.db"
	DbOptions = "?_pragma=busy_timeout(5000)&_pragma=foreign_keys(1)"
)

type DB struct {
	db *sql.DB
}

func NewDB(path string) *DB {
	var err error
	log.Info("Opening SQLite db", "path", path)
	db, err := sql.Open("sqlite", path+DbOptions)
	if err != nil {
		panic(err)
	}
	d := &DB{db: db}
	err = d.Migrate()
	if err != nil {
		panic(err)
	}
	return d
}

func (m *DB) Migrate() error {
	return m.WithInTx(func(tx *sql.Tx) error {

		return nil
	})
}

func (m *DB) WithInTx(f func(tx *sql.Tx) error) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	tx, err := m.db.BeginTx(ctx, nil)
	if err != nil {
		log.Error("error starting transaction", "error", err)
		return err
	}
	for {
		err = f(tx)
		if err != nil {
			serr, ok := err.(*sqlite.Error)
			if ok && serr.Code() == sqlitelib.SQLITE_BUSY {
				continue
			}
			log.Error("error in trasaction", "error", err)
			return err
		}
		err = tx.Commit()
		if err != nil {
			log.Error("error commiting transaction", "error", err)
			return err
		}
		break
	}
	return nil
}

// Close implements db.DB.
func (m *DB) Close() error {
	return nil
}
