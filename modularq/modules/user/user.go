package user

import (
	"encoding/json"
	"fmt"
	"log"
	"modularq/app"
	"modularq/pkg/logger"
	"net/http"

	"github.com/nats-io/nats.go"
)

var _ app.Module = (*UserModule)(nil)

type UserModule struct {
	config map[string]any
	logger *logger.Logger
}

func NewUserModule(logger *logger.Logger) *UserModule {
	return &UserModule{
		logger: logger,
	}
}

// Config implements app.Module.
func (m *UserModule) Config() map[string]any {
	return m.config
}

// Init implements app.Module.
func (m *UserModule) Init(config map[string]any) error {
	m.config = config
	log.Printf("Initializing %s module with config: %+v", m.Name(), m.config)
	return nil
}

// HTTPHandlers implements app.Module.
func (m *UserModule) HTTPHandlers(pub *app.Publisher) []app.HTTPHandler {
	return []app.HTTPHandler{
		{
			Method: "POST",
			Path:   "/signup",
			Handler: func(w http.ResponseWriter, r *http.Request) {
				var user struct {
					Name  string `json:"name"`
					Email string `json:"email"`
				}
				if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
					return
				}

				pub.Publish("user.created", fmt.Appendf(nil, "User %s created", user.Name))
				w.WriteHeader(http.StatusCreated)
				fmt.Fprintf(w, "User %s created", user.Name)
			},
		},
	}
}

// MsgHandlers implements app.Module.
func (m *UserModule) MsgHandlers(pub *app.Publisher) []app.MsgHandler {
	return []app.MsgHandler{
		{
			Subject: "user.created",
			Handler: func(msg *nats.Msg) {
				log.Printf("Received message on %s: %s", msg.Subject, string(msg.Data))
			},
		},
	}
}

// Name implements app.Module.
func (m *UserModule) Name() string {
	return "user"
}
