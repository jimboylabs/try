package ui

import (
	"embed"
	"encoding/json"
	"fmt"
	"html/template"
	"modularq/app"
	"modularq/pkg/logger"
	"net/http"
	"path/filepath"
	"strings"

	datastar "github.com/starfederation/datastar/sdk/go"
)

var _ app.Module = (*UIModule)(nil)

//go:embed html/*.tmpl
var templateFS embed.FS

//go:embed all:static
var staticFiles embed.FS

type UIModule struct {
	config map[string]any
	logger *logger.Logger
}

func NewUIModule(logger *logger.Logger) *UIModule {
	return &UIModule{
		logger: logger,
	}
}

// Init implements app.Module.
func (m *UIModule) Init(config map[string]any) error {
	m.config = config
	return nil
}

// Config implements app.Module.
func (m *UIModule) Config() map[string]any {
	return m.config
}

// HTTPHandlers implements app.Module.
func (m *UIModule) HTTPHandlers(pub *app.Publisher) []app.HTTPHandler {
	return []app.HTTPHandler{
		{
			Method: "GET",
			Path:   "/static/{file}",
			Handler: func(w http.ResponseWriter, r *http.Request) {
				modulePath := fmt.Sprintf("/%s/", m.Name())

				if r.URL.Path == modulePath || r.URL.Path == strings.TrimSuffix(modulePath, "/") {
					index, err := staticFiles.ReadFile("static/index.html")
					if err != nil {
						http.Redirect(w, r, modulePath, http.StatusFound)
						return
					}

					w.Header().Set("Content-Type", "text/html")
					w.Write(index)
					return
				}

				filePath := strings.TrimPrefix(r.URL.Path, modulePath)
				filePath = filepath.Clean("/" + filePath) // Clean the path to prevent directory traversal attacks

				file, err := staticFiles.ReadFile("static" + filePath)
				if err != nil {
					http.Redirect(w, r, modulePath, http.StatusFound)
					return
				}

				ext := filepath.Ext(filePath)
				switch ext {
				case ".css":
					w.Header().Set("Content-Type", "text/css")
				case ".js":
					w.Header().Set("Content-Type", "application/javascript")
				case ".png", ".jpg", ".jpeg", ".gif":
					w.Header().Set("Content-Type", "image/"+strings.TrimPrefix(ext, "."))
				case ".html":
					w.Header().Set("Content-Type", "text/html")
				default:
					w.Header().Set("Content-Type", "application/octet-stream")
				}

				w.Write(file)
			},
		},
		{
			Method: "GET",
			Path:   "/auth/register",
			Handler: func(w http.ResponseWriter, r *http.Request) {
				files := []string{
					"html/index.page.tmpl",
				}

				ts, err := template.ParseFS(templateFS, files...)
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
					return
				}

				err = ts.Execute(w, map[string]any{
					"message": "Hello, World!",
				})
				if err != nil {
					http.Error(w, err.Error(), http.StatusInternalServerError)
				}
			},
		},
		{
			Method: "GET",
			Path:   "/api/question",
			Handler: func(w http.ResponseWriter, r *http.Request) {
				sse := datastar.NewSSE(w, r)
				question := "What do you put in a toaster ?"
				answer := "bread"
				sse.MergeFragments(fmt.Sprintf(`<div id="question">%s</div>`, question))
				sse.MergeSignals(json.RawMessage(`{"answer": "` + answer + `"}`))
			},
		},
		{
			Method: "POST",
			Path:   "/api/submit",
			Handler: func(w http.ResponseWriter, r *http.Request) {
				data := struct {
					Answer string `json:"answer"`
				}{}

				if err := json.NewDecoder(r.Body).Decode(&data); err != nil {
					http.Error(w, err.Error(), http.StatusBadRequest)
					return
				}

				sse := datastar.NewSSE(w, r)
				result := `<div id="result">The correct answer is bread</div>`
				if data.Answer == "bread" {
					result = `<div id="result">This is correct!</div>`
				}

				sse.MergeFragments(result)
			},
		},
	}
}

// MsgHandlers implements app.Module.
func (m *UIModule) MsgHandlers(pub *app.Publisher) []app.MsgHandler {
	return []app.MsgHandler{}
}

// Name implements app.Module.
func (m *UIModule) Name() string {
	return "ui"
}
