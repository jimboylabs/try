package app

import (
	"fmt"
	"modularq/pkg/logger"
	"net/http"
	"os"
	"path"
	"time"

	"github.com/nats-io/nats-server/v2/server"
	"github.com/nats-io/nats.go"
	"gopkg.in/yaml.v3"
)

type Config struct {
	Name    string `yaml:"name"`
	NATS    NATSConfig
	HTTP    HTTPConfig
	Modules map[string]ModuleConfig `yaml:"modules"`
}

type NATSConfig struct {
	Embedded bool `yaml:"embedded"`
}

type HTTPConfig struct {
	Port int `yaml:"port"`
}

type ModuleConfig struct {
	Enabled bool           `yaml:"enabled"`
	Config  map[string]any `yaml:"config"`
}

type App struct {
	config Config
	nats   *nats.Conn
	http   *http.ServeMux
	logger *logger.Logger
}

func New(configPath string, logger *logger.Logger) (*App, error) {
	var config Config
	data, err := os.ReadFile(configPath)
	if err != nil {
		return nil, err
	}
	err = yaml.Unmarshal(data, &config)
	if err != nil {
		return nil, err
	}

	a := &App{
		config: config,
		http:   http.NewServeMux(),
		logger: logger,
	}

	if config.NATS.Embedded {
		ns, err := startEmbeddedNatsServer(config.Name, config.NATS)
		if err != nil {
			return nil, err
		}
		a.nats, err = connectToEmbeddedNATS(config.Name, ns, config.NATS)
		if err != nil {
			return nil, err
		}
	} else {
		a.nats, err = nats.Connect(nats.DefaultURL)
		if err != nil {
			return nil, err
		}
	}

	return a, nil
}

func (a *App) AddModule(m Module) {
	if m.Config() == nil || m.Config()["enabled"] == true {
		publisher := &Publisher{nc: a.nats}

		modulePath := fmt.Sprintf("/%s/", m.Name())

		for _, handler := range m.HTTPHandlers(publisher) {
			fullPath := modulePath
			if handler.Path != "/" {
				fullPath = path.Join(modulePath, handler.Path)
			}

			a.http.HandleFunc(fullPath, handler.Handler)
			a.logger.Infof("Routes of module %s with path %s was registered", m.Name(), fullPath)

			// Add a handler for the path without trailing slash to the path with slash
			if fullPath[len(fullPath)-1] == '/' {
				pathWithoutSlash := fullPath[:len(fullPath)-1]
				a.http.HandleFunc(pathWithoutSlash, func(w http.ResponseWriter, r *http.Request) {
					http.Redirect(w, r, fullPath, http.StatusMovedPermanently)
				})
				a.logger.Infof("Redirect from %s to %s was registered", pathWithoutSlash, fullPath)
			}
		}
		for _, handler := range m.MsgHandlers(publisher) {
			sub, err := a.nats.Subscribe(handler.Subject, handler.Handler)
			if err != nil {
				a.logger.Fatalf("Failed to subscribe to %s: %v", handler.Subject, err)
			}
			a.logger.Infof("Subscribed to %s", sub.Subject)
		}
	}
}

func (a *App) Run() error {
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%d", a.config.HTTP.Port),
		Handler: a.http,
	}
	a.logger.Infof("Starting server on port %s", srv.Addr)
	return srv.ListenAndServe()
}

func startEmbeddedNatsServer(appName string, opts NATSConfig) (*server.Server, error) {
	serverOpts := &server.Options{
		ServerName:      fmt.Sprintf("%s-nats-server", appName),
		DontListen:      opts.Embedded,
		JetStream:       true,
		JetStreamDomain: appName,
	}

	ns, err := server.NewServer(serverOpts)
	if err != nil {
		return nil, err
	}

	if opts.Embedded {
		ns.ConfigureLogger()
	}

	ns.Start()

	if !ns.ReadyForConnections(5 * time.Second) {
		return nil, nats.ErrTimeout
	}

	return ns, nil
}

func connectToEmbeddedNATS(appName string, ns *server.Server, opts NATSConfig) (*nats.Conn, error) {
	clientOpts := []nats.Option{
		nats.Name(fmt.Sprintf("%s-nats-client", appName)),
	}
	if opts.Embedded {
		clientOpts = append(clientOpts, nats.InProcessServer(ns))
	}
	nc, err := nats.Connect(nats.DefaultURL, clientOpts...)
	if err != nil {
		return nil, err
	}
	return nc, nil
}
