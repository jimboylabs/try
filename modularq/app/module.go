package app

import (
	"net/http"

	"github.com/nats-io/nats.go"
)

type Module interface {
	Name() string
	Config() map[string]any
	Init(config map[string]any) error
	HTTPHandlers(pub *Publisher) []HTTPHandler
	MsgHandlers(pub *Publisher) []MsgHandler
}

type HTTPHandler struct {
	Method  string
	Path    string
	Handler func(http.ResponseWriter, *http.Request)
}

type MsgHandler struct {
	Subject string
	Handler func(*nats.Msg)
}

type Publisher struct {
	nc *nats.Conn
}

func (p *Publisher) Publish(subject string, data []byte) error {
	msg := &nats.Msg{
		Subject: subject,
		Data:    data,
	}
	return p.nc.PublishMsg(msg)
}
