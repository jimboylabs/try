package main

import (
	"log"
	"modularq/app"
	"modularq/modules/ui"
	"modularq/modules/user"
	"modularq/pkg/logger"
	"os"
)

func main() {
	logger := logger.NewLogger(os.Stdout, os.Stderr)

	appInstance, err := app.New("config.yaml", logger)
	if err != nil {
		log.Fatalf("Failed to create app: %v", err)
	}

	userModule := user.NewUserModule(logger)
	if err := userModule.Init(map[string]any{"enabled": true}); err != nil {
		log.Fatalf("Failed to init user module: %v", err)
	}
	appInstance.AddModule(userModule)

	uiModule := ui.NewUIModule(logger)
	if err := uiModule.Init(map[string]any{"enabled": true}); err != nil {
		log.Fatalf("Failed to init ui module: %v", err)
	}
	appInstance.AddModule(uiModule)

	//TODO: handle graceful shutdown
	if err := appInstance.Run(); err != nil {
		log.Fatalf("Failed to run app: %v", err)
	}
}
