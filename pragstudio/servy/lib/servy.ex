defmodule Servy do
  def hello(name) do
    "Hello, #{name}"
  end
end

# IO.pu(s Servy.hello("Jimmy))")
