import { drizzle } from "drizzle-orm/libsql";
import { createClient } from "@libsql/client";

const turso = createClient({
  url: Deno.env.get("DATABASE_URL"),
});
export const db = drizzle(turso);
