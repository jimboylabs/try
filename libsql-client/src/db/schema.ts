import { integer, sqliteTable, text } from "drizzle-orm/sqlite-core";

export const dinosaurs = sqliteTable("dinosaurs", {
  id: integer("id").primaryKey(),
  name: text("name").notNull(),
});
