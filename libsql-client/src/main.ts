import { dinosaurs } from "./db/schema.ts";
import { db} from "./store.ts";

if (import.meta.main) {
    const result = await db.select().from(dinosaurs);
    console.log({result});
}